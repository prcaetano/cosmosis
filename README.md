# cosmoSIS - emulatortrainer sampler

This is a fork of CosmoSIS (https://bitbucket.org/joezuntz/cosmosis), implementing an interface to enable emulation of CosmoSIS modules with gaussian processes directly, by adding a new sampler, emulatortrainer, responsible for the training, and a few classes to implement emulated modules. For now, the underlying gaussian process emulation implementation is from [george](https://george.readthedocs.io/).

Documentation for CosmoSIS can be found at https://bitbucket.org/joezuntz/cosmosis/wiki. The specific features here are not well documented yet (for some examples of use, look at https://gitlab.com/prcaetano/monografia-analise/, particularly the jobs directory).
