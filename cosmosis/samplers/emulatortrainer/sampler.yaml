name: "emulatortrainer"
version: "0.1.0"
parallel: parallel
purpose: "Training emulator of emulated versions of cosmosis modules."
attribution: ["Pedro Rangel Caetano, Dan Foreman-Mackey and contributors"]
cite:
    - "http://arxiv.org/abs/1403.6015"

explanation: >
    Emulators are codes that somehow interpolates a given function.
    This is useful to speed up slow modules by first running the module
    on some points sampled from the parameter space and then using
    the emulator to return approximations of the function calculated
    by the module on any point in (some subset of) the parameter space.

    This sampler is intented to be used together with modules constructed
    by subclassing the EmulatedModule class (defined on 
    cosmosis.runtime.emulated_module), which implements the interface
    between cosmosis modules and emulators (this last one described 
    by the interface class Emulator, defined on cosmosis.runtime.emulator).

    The only actual emulator class currently implemented is the GPEmulator,
    which uses the [george](https://george.readthedocs.io/en/latest/)
    code and is a Gaussian Process emulator, but is is possible to implement
    others by subclassing the Emulator class.

    Notice the on the configuration options list above, all besides 
    emulated_module should instead be specified on the section corresponding 
    to the module being emulated. They are listed here by lack of a better place.

installation: >
    the sampling of points in parameter space is done using the
    package [lhsmdu](https://github.com/sahilm89/lhsmdu](https://github.com/sahilm89/lhsmdu)
    which is not available on pip but can be installed by clonning the
    repository and running inside its root directory

    python setup.py install

    you also need to install any packages needed by the Emulator used.
    For the GPEmulator class the only added requirement is george,
    which can be installed by

    pip install george  #to install centrally, may require sudo
    
    pip install george --user #to install just for you

# List of configuration options for this sampler
params:
    emulated_module: (str) name of the emulated module to train
    nsamples: (integer) number of points in parameter space to sample
    nrepeats: (integer) number of repeated runs for each point in parameter space (useful if there is stocasticity on the module calculations). The variance over these repeats is added in quadrature to the noise_level specified.
    nstep: (integer) number of samples to calculate at each 
    walkers: (integer) number of walkers in the space
    samples: (integer) number of jumps to attempt per walker
    nsteps: (integer) number of samples to compute at each step (that is, each pool.map call)
    seed: (integer) seed used for the random generation of initial conditions when fitting the emulator
    noise_level: (float or str) if float, the uncertainty on the calculated values of the module function being emulated. If str, path to file holding an array containing the uncertainties at each calculated value
    xmin: (float) minimum value of the dependent variable of the emulated function. If not specified, the dependent variable values will be read from the observed data instead (using the build_data method of the EmulatedModule object used)
    xmax: (float) maximum value of the dependent variable
    nx: (float) number of values of the dependent variable
    xscale: (str) lin if the values of the dependent variable should be linearly space, log if logarithmically spaced
    training_file: (str) path to fits file used for holding the compute values of the non emulated module
    emulator_file: (str) path to file used for holding the pickled emulator object, already trained
    is_trained: (bool) whether the module have already been trained or not. Needs to be True
    noi


