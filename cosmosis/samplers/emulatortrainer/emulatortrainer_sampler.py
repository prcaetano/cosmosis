from __future__ import print_function
from builtins import zip
from builtins import map
from builtins import str
import os
import sys
import numpy as np
import pickle
import lhsmdu
from astropy.table import Table
from astropy.io import fits
from .. import ParallelSampler
from cosmosis.runtime.emulated_module import EmulatedModule
from cosmosis.datablock import names
from cosmosis.datablock.cosmosis_py.errors import BlockWrongValueType
from cosmosis.runtime.pipeline import LikelihoodPipeline, Pipeline
from copy import deepcopy as copy
from itertools import repeat
from contextlib import contextmanager
from scipy.optimize import minimize
from scipy.interpolate import interp1d


# Context for redirection of stdout
# (https://stackoverflow.com/a/22434262/4279)
def fileno(file_or_fd):
    fd = getattr(file_or_fd, 'fileno', lambda: file_or_fd)()
    if not isinstance(fd, int):
        raise ValueError("Expected a file (`.fileno()`) or a file descriptor")
    return fd

@contextmanager
def stdout_redirected(to=os.devnull, stdout=None):
    if stdout is None:
       stdout = sys.stdout

    stdout_fd = fileno(stdout)
    # copy stdout_fd before it is overwritten
    #NOTE: `copied` is inheritable on Windows when duplicating a standard stream
    with os.fdopen(os.dup(stdout_fd), 'wb') as copied:
        stdout.flush()  # flush library buffers that dup2 knows nothing about
        try:
            os.dup2(fileno(to), stdout_fd)  # $ exec >&to
        except ValueError:  # filename
            with open(to, 'wb') as to_file:
                os.dup2(to_file.fileno(), stdout_fd)  # $ exec > to
        try:
            yield stdout # allow code to be run with the redirected stdout
        finally:
            # restore stdout to its previous value
            #NOTE: dup2 makes stdout_fd inheritable unconditionally
            stdout.flush()
            os.dup2(copied.fileno(), stdout_fd)  # $ exec >&copied


class EmulatorTrainerSampler(ParallelSampler):
    needs_output = False

    def config(self):
        global trainer
        trainer = self

        self.emulated_module = self.read_ini("emulated_module", str)
        self.like_name = self.read_ini("likelihood", str)
        self.do_bayesian_optimization = self.read_ini("do_bayesian_optimization", bool)
        if self.do_bayesian_optimization:
            self.alpha_function = self.read_ini("alpha_function", str, default="linear")

            if self.alpha_function == "linear":
                self.alpha_nu = self.read_ini("alpha_nu", float, default=0.97)
                self.alpha_initial = self.read_ini("alpha_initial", float, default=1.)
                self.alpha_final = self.read_ini("alpha_final", float, default=np.sqrt(0.4))

            elif self.alpha_function == "constant":
                self.alpha = self.read_ini("alpha_constant", float, default=1.)

            elif self.alpha_function == "no-regret":
                self.alpha_nu = self.read_ini("alpha_nu", float, default=1.)
                self.alpha_delta = self.read_ini("alpha_delta", float, default=0.1)

            else:
                raise RuntimeError("Invalid choice for alpha_function."
                                   "Should be one of linear, constant and no-regret")

            self.beta_function = self.read_ini("beta_function", str, default="linear")

            if self.beta_function == "linear":
                self.beta_initial = self.read_ini("beta_initial", float, default=0.)
                self.beta_final   = self.read_ini("beta_final", float, default=1.)
            elif self.beta_function == "constant":
                beta              = self.read_ini("beta_constant", float, default=1.)
                self.beta_initial = beta
                self.beta_final   = beta
            else:
                raise RuntimeError("Invalid choice for beta_function."
                                   "Should be one of linear and constant")

            self.sampling_points_fname = self.read_ini("sampling_points_fname", str,
                                                       default="sampling_points.npy")
            self.acquisition_maxima_fname = self.read_ini("acquisition_maxima_fname", str,
                                                          default="acquisition_maxima.npy")

            self.covariance_key = self.read_ini("covariance_key", str, default="")
            if self.covariance_key != "":
                self.covariance_section, self.covariance_key = self.covariance_key.split()
                self.add_theo_cov = True
            else:
                self.add_theo_cov = False
        else:
            self.add_theo_cov = False

        self.emulated_pipeline = self.read_ini("emulated_pipeline", str, default="")
        self.emulated_pipeline_shortcut = self.read_ini("emulated_pipeline_shortcut", str, default="")
        if self.emulated_pipeline == "":
            self.emulated_pipeline = self.ini.get("pipeline", "modules")
        if self.emulated_pipeline_shortcut == "":
            self.emulated_pipeline_shortcut = self.ini.get("pipeline", "shortcut", "")

        self.cache_directory = self.read_ini("cache_directory", str, default="")
        self.do_cache = self.cache_directory != "" and os.path.exists(self.cache_directory)
        if self.do_cache:
            self.cached_params_fname = os.path.join(self.cache_directory, "params.npy")
            self.cached_yvals_fname = os.path.join(self.cache_directory, "yvals.npy")
            self.cached_yerrs_fname = os.path.join(self.cache_directory, "yerrs.npy")

        module_names = list(map(str, self.pipeline.modules))
        if self.emulated_module not in module_names:
            raise RuntimeError("The module choosen for emulation"
                               "is not on the pipeline!")

        module_idx = module_names.index(self.emulated_module)
        self.emulated_module_obj = self.pipeline.modules[module_idx].data
        if not isinstance(self.emulated_module_obj, EmulatedModule):
            raise RuntimeError("Could not load EmulatedModule class for"
                               "the emulated module."
                               "Are you sure your module is compatible with"
                               "the emulatortrainer sampler, i.e., was coded"
                               "by subclassing the EmulatedModule class?")

        self.emulator_generator = self.emulated_module_obj.emulator_generator
        if self.emulator_generator == None:
            raise RuntimeError("Emulator class not specified."
                               "You need to specify this when"
                               "subclassing EmulatedModule.")

        self.training_file = self.ini.get(self.emulated_module, "training_file",
                                          fallback="training.fits")
        self.emulator_file = self.ini.get(self.emulated_module, "emulator_file",
                                          fallback="training.pkl")
        if self.ini.get(self.emulated_module, "sampling_design", fallback="") != "":
            self.load_design = True
            self.design_fname = self.ini.get(self.emulated_module, "sampling_design", fallback="")
        else:
            self.load_design = False

        self.is_trained = self.ini.getboolean(self.emulated_module, "is_trained")
        if self.is_trained:
            raise RuntimeError("is_trained is set to True. Aborting.")

        self.nsamples = self.ini.getint(self.emulated_module, "nsamples")
        self.nrepeats = self.ini.getint(self.emulated_module, "nrepeats")
        self.nstep = self.ini.getint(self.emulated_module, "nstep")
        if self.do_bayesian_optimization:
            self.ntotal = self.ini.getint(self.emulated_module, "ntotal")
        else:
            self.ntotal = self.nsamples
        self.seed = self.ini.getint(self.emulated_module, "seed", fallback=42)

        self.ndim = self.pipeline.nvaried
        self.varied_params = self.pipeline.varied_params
        self.fixed_params = self.pipeline.fixed_params
        self.limits = np.array(list(
                                    map(lambda x: list(x.limits),
                                        self.varied_params)
                                   ))
        self.start_values = np.array(list(
                                          map(lambda x: x.start,
                                              self.varied_params)))
        self.fixed_params_values = np.array(list(
                                                 map(lambda x: x.limits[0],
                                                     self.fixed_params)
                                                ))
        self.fixed_params_names = list(map(str, self.fixed_params))
        self.varied_params_names = list(map(str, self.varied_params))

        if self.do_bayesian_optimization:
            self.acquisition_cov_fname = self.ini.get(self.emulated_module, "acquisition_covariance")
            n_varied = len(self.varied_params)
            self.acquisition_covariance = np.loadtxt(self.acquisition_cov_fname).reshape(n_varied, n_varied)

        self.training_file_exists = os.path.isfile(self.training_file)

        self.x, _ = self.emulated_module_obj.build_data()
        if self.ini.has_option(self.name, "xmin"):
            xmin = self.read_ini("xmin", float)
            xmax = self.read_ini("xmax", float)
            nx = self.read_ini("nx", float)
            xscale = self.read_ini("xscale", str, "lin")
            if "log" in xscale.lower():
                self.x = np.logspace(np.log10(xmin), np.log10(xmax), nx)
            else:
                self.x = np.linspace(xmin, xmax, nx)
        self.nx, = self.x.shape

        # Read noise_level (independent of params, but may depend on x)
        try:
            self.yerr_level = self.ini.getfloat(self.emulated_module, "noise_level", fallback=0.)
        except ValueError:
            yfile = self.ini.get(self.emulated_module, "noise_level")
            xerr, yerr = np.loadtxt(yfile, unpack=True)
            interp_err = interp1d(xerr, yerr, fill_value="extrapolate")
            yerr = interp_err(self.x)
            self.yerr_level = yerr
        if (np.array(self.yerr_level) != 0.).any():
            print("Adding noise level specified on pipeline to y values variance.")
        self.yerr_level_relative = self.ini.getboolean(self.emulated_module, "is_noise_level_relative", fallback=False)

        self.y_and_yvar = np.zeros((self.ntotal * self.nrepeats, 2, self.nx)) * np.nan
        self.y = self.y_and_yvar[:,0,:]
        self.yvar = self.y_and_yvar[:,1,:]
        self.yvar[:] = 0.
        self.yerr = np.zeros_like(self.y)

        if self.nrepeats > 1:
            self.y_and_yvar_mean = np.zeros((self.ntotal, 2, self.nx)) * np.nan
        else:
            self.y_and_yvar_mean = self.y_and_yvar
        self.y_mean = self.y_and_yvar_mean[:,0,:]
        self.yvar_mean = self.y_and_yvar_mean[:,1,:]
        self.yvar_mean[:] = 0.
        self.yerr_mean = np.zeros_like(self.y_mean)

        self.ndone = 0
        self.ndone_fitting = 0
        self.sample_points = None
        self.converged = False

        # Some functions that will be needed for computationally intensive
        # tasks latter have to be defined here, so that they are in scope
        # for both the parent and children processes
        global task
        def task(params):
            """
            Runs pipeline for each parameter in params and returns array
            shaped as (len(params), len(theory)) containing values of theory
            computed for the emulated module.
            """
            params = np.atleast_2d(params)
            theories = []
            theories_var = []

            for param in params:
                # Load values from cache if already computed
                if self.do_cache:
                    try:
                        idx = np.argwhere(np.isclose(np.load(self.cached_params_fname), param).all(axis=1)).flatten()[0]
                        theory = np.load(self.cached_yvals_fname)[idx]
                        theory_err = np.load(self.cached_yerrs_fname)[idx]
                        theories.append(theory)
                        theories_var.append(theory_err**2)
                        print("Loading parameters already computed from cache.")
                        continue
                    except:
                        pass

                param = np.array(param).flatten()
                data = self.pipeline.run_parameters(param)
                theory = data.get(self.emulated_module_obj.y_section,
                                  self.emulated_module_obj.y_name)
                if (self.emulated_module_obj.y_section, self.emulated_module_obj.yerr_name) in data.keys():
                    theory_err = data.get(self.emulated_module_obj.y_section,
                                          self.emulated_module_obj.yerr_name)
                else:
                    theory_err = np.zeros_like(theory)

                if self.do_cache:
                    try:
                        np.save(self.cached_params_fname,
                                np.atleast_2d(np.append(np.load(self.cached_params_fname), np.atleast_2d(param), axis=0)))
                        np.save(self.cached_yvals_fname,
                                np.atleast_2d(np.append(np.load(self.cached_yvals_fname), np.atleast_2d(theory), axis=0)))
                        np.save(self.cached_yerrs_fname,
                                np.atleast_2d(np.append(np.load(self.cached_yerrs_fname), np.atleast_2d(theory_err), axis=0)))
                    except:
                        if (not os.path.exists(self.cached_params_fname)
                            and not os.path.exists(self.cached_yvals_fname)
                            and not os.path.exists(self.cached_yerrs_fname)):
                            np.save(self.cached_params_fname, np.atleast_2d(param))
                            np.save(self.cached_yvals_fname, np.atleast_2d(theory))
                            np.save(self.cached_yerrs_fname, np.atleast_2d(theory_err))

                theories.append(theory)
                theories_var.append(theory_err**2)

            ret = np.array([theories, theories_var])
            return ret

        global emulator_fit
        def emulator_fit(args):
            args, emulator = args
            return emulator.fit(args, suppress_errors=True)


    def execute(self): # only master arrives here

        # Load/compute sampling points for current slice to be run
        self.setup_sampling()

        # Run simulation and compute means/covariances over repetitions
        self.run_simulation()

        # Setup emulator for fitting
        self.setup_fitting()

        # Runs fitting and dumps emulator
        self.run_fitting()


    def is_converged(self):
        return self.converged


    def setup_sampling(self):
        """
        Computes/loads sampling points, initially from a latin hypercube
        (or an arbitrary design given by the user), and then by optimizing
        the acquisition function.
        """
        if self.sample_points is None:   # <- First run
            self.sample_points = np.zeros((self.ntotal, self.ndim))

            if self.training_file_exists:
                self.load_training_file()
                if self.ndone >= self.nsamples:
                    self.setup_fitting()
                    self.run_fitting()
                    self.setup_sampling()
            elif self.load_design:
                print("Loading sampling design from {}...".format(self.design_fname))
                self.sample_points[:self.nsamples] = np.load(self.design_fname)
            else:
                print("Generating sampling design...")
                lh = lhsmdu.sample(self.ndim, self.nsamples)
                left_limits = self.limits[:,0]
                right_limits = self.limits[:,1]
                rescaled_lh = np.diag(right_limits - left_limits) * lh \
                        + np.diag(left_limits) * np.ones_like(lh)
                self.sample_points[:self.nsamples] = np.array(rescaled_lh).T

        else:
            if self.ndone >= self.nsamples and not self.converged:
                optimized_batch = self.get_optimized_sampling_points()
                self.sample_points[self.ndone:self.ndone+len(optimized_batch)] = \
                        optimized_batch


    def run_simulation(self):
        """
        Runs nonemulated pipeline for a slice of self.sample_points.
        """
        if not self.converged:
            # Cut a slice of sample_points
            endslice = min(self.ndone + self.nstep,
                           self.nsamples if self.ndone < self.nsamples else self.ntotal)
            xslice = np.arange(self.ndone, endslice)
            xslice = np.repeat(np.atleast_2d(xslice),
                               self.nrepeats, axis=0).flatten()
            yslice = np.array([
                               np.arange(self.ndone*self.nrepeats+i,
                                         endslice*self.nrepeats,
                                         self.nrepeats) for i in range(self.nrepeats)
                              ]).flatten()

            # Actually compute the emulated values
            if self.pool:
                self.y_and_yvar[yslice] = np.array(
                                                    self.pool.map(task,
                                                                  self.sample_points[xslice])
                                                   ).reshape(-1,2,self.nx)
            else:
                self.y_and_yvar[yslice] = np.array(
                                                    list(map(task,
                                                             self.sample_points[xslice]))
                                                   ).reshape(-1,2,self.nx)

            # Summing noise level provided with variance from pipeline
            self.yerr[yslice] = np.sqrt(self.yvar[yslice] + self.yerr[yslice]**2)

            xslice = np.arange(self.ndone, endslice)
            yslice = np.arange(self.ndone * self.nrepeats, endslice * self.nrepeats)
            yslice_mean = np.arange(self.ndone, endslice)
            self.compute_means_and_variances(xslice=xslice, yslice=yslice, yslice_mean=yslice_mean)

            # Updates the count
            self.ndone = endslice

            # Saves progress until here
            self.save_training_file(endslice)


    def compute_means_and_variances(self, xslice, yslice, yslice_mean):
        """
        If nrepeats > 1, computes y_mean and yvar_mean by taking mean
        over repetitions, and also calculate yerr_mean by combining
        yvar_mean and yerr_level.

        Parameters:
            xslice: list of int
                indexes describing lines of sample_points where the mean should be computed
            yslice: list of int
                indexes describing lines of self.y and self.yvar where the mean should be computed
            yslice_mean: list of int
                indexes describing lines of y_mean, yvar_mean and yerr_mean where the means
                should be saved
        """
        if self.nrepeats > 1:
            assert len(yslice) == len(yslice_mean) * self.nrepeats
            self.y_mean[yslice_mean] = np.ma.masked_invalid([
                                                             self.y[yslice][k:k+self.nrepeats]
                                                             for k in np.arange(0, len(yslice_mean))*self.nrepeats
                                                            ]).mean(axis=1).filled(np.nan)
            cov = np.array([
                            np.ma.cov(np.ma.masked_invalid(self.y[yslice][k:k+self.nrepeats].T)).filled(np.nan)
                            for k in np.arange(0, len(yslice_mean))*self.nrepeats
                           ])
            self.yvar_mean[yslice_mean] = np.array([
                                                    [cov[i,j,j] for j in range(self.nx)]
                                                    for i in np.arange(0, len(yslice_mean))
                                                   ])
        if self.yerr_level_relative:
            yerr_level = self.yerr_level*self.y_mean[yslice_mean]
        else:
            yerr_level = self.yerr_level
        self.yerr_mean[yslice_mean] = np.sqrt(self.yvar_mean[yslice_mean] + yerr_level**2)


    def setup_fitting(self):
        """
        Instanciates emulator and loads initial conditions for the fitting.
        """
        if self.ndone >= self.nsamples and not self.converged:
            if self.emulator_generator == None:
                raise RuntimeError("Emulator class not specified."
                                   "You need to specify this when"
                                   "overriding EmulatedModule.")

            # Drop parameters not simulated
            mask_not_simulated = np.isnan(self.y_mean).all(axis=1)
            if mask_not_simulated.any():
                print("For some values of the parameters the simulation failed completely. "
                      "Dropping those.")
            y_mean = self.y_mean[~mask_not_simulated]
            yvar_mean = self.yvar_mean[~mask_not_simulated]
            yerr_mean = self.yerr_mean[~mask_not_simulated]
            sample_points = self.sample_points[~mask_not_simulated]

            print("Instanciating emulator...")
            # Instanciating emulator
            self.emulator = self.emulator_generator(self.x, y_mean,
                                                    yerr_mean,
                                                    sample_points,
                                                    self.varied_params_names,
                                                    self.seed)
            print("Done.")

            # Getting initial conditions and allocating trained_scores
            # and trained_parameters
            self.initial_conditions = self.emulator.initial_conditions

            self.num_initial_conditions, self.num_emulator_parameters = \
                    self.initial_conditions.shape

            self.num_emulators = self.emulator.n_of_emulators

            self.trained_scores = np.nan * np.ones((self.num_initial_conditions,
                                                    self.num_emulators))
            self.trained_parameters = np.nan * np.ones((self.num_initial_conditions,
                                                        self.num_emulators,
                                                        self.num_emulator_parameters))


    def run_fitting(self):
        """
        Fits the emulator for all initial conditions, and chooses the best one.
        Also saves the resulting (pickled) emulator object.
        """
        # Only fits if nsamples samples already computed
        if self.ndone >= self.nsamples and not self.converged:

            # Get scores and emulator parameters
            if self.pool:
                pars = list(zip(self.initial_conditions,
                                repeat(self.emulator)))
                parameters_and_scores = self.pool.map(emulator_fit,
                                                      pars)
            else:
                pars = list(zip(self.initial_conditions,
                                repeat(self.emulator)))
                parameters_and_scores = list(map(emulator_fit,
                                                 pars))

            for idx, (parameters, score) in enumerate(parameters_and_scores):
                self.trained_parameters[idx] = parameters
                self.trained_scores[idx] = score

            idx_max = np.argmax(self.trained_scores, axis=0), np.arange(self.num_emulators)
            self.fitted_parameters = self.trained_parameters[idx_max]

            print("END EMULATOR FITTING")
            print("Maximum likelihoods: ", self.trained_scores[idx_max])
            print("Parameters: ", self.fitted_parameters)

            self.emulator.set_parameters(self.fitted_parameters)

            # Saves pickled emulator
            if not self.converged:
                emulator_fname = self.emulator_file
                if "." in emulator_fname:
                    parts = emulator_fname.split(".")
                    emulator_fname = ".".join(parts[:-1]) + "_ndone" + str(self.ndone) + "." + parts[-1]
                else:
                    emulator_fname += "_ndone" + str(self.ndone)

                with open(emulator_fname, "wb") as f:
                    pickle.dump(self.emulator, f)

            with open(self.emulator_file, "wb") as f:
                pickle.dump(self.emulator, f)

            # (Re)load emulated pipeline object to use when computing acquisition function
            self.setup_emulated_pipeline()

            # Check convergence
            self.converged = self.ndone == self.ntotal

    def get_optimized_sampling_points(self):
        """
        Gets next batch of sampling points by optimizing the acquisition function
        """
        # Drop 10 % of parameter space near limit borders
        limits = self.limits.copy()
        limits[:,0] = self.limits[:,0] + 0.05 * (self.limits[:,1] - self.limits[:,0])
        limits[:,1] = self.limits[:,1] - 0.05 * (self.limits[:,1] - self.limits[:,0])

        # 5sigma range around start_values (assumed to be max like)
        max_5sigma = self.start_values + 5*np.sqrt(np.diag(self.acquisition_covariance))
        min_5sigma = self.start_values - 5*np.sqrt(np.diag(self.acquisition_covariance))

        # Restrict to 3sigma around maximum if it is stricter than original limits
        limits[:,0] = np.maximum(min_5sigma, limits[:,0])
        limits[:,1] = np.minimum(max_5sigma, limits[:,1])

        def get_alpha_linear(t):
            """
            Gets relative importance of exploration/exploitation in acquisition function
            (following Rogers 2019)
            """
            a = self.alpha_initial**2
            b = a - self.alpha_final**2
            return np.sqrt(a - b * (t - self.nsamples) / (self.ntotal - self.nsamples))

        def get_alpha_no_regret(t):
            """
            Gets relative importance of exploration/exploitation in acquisition function
            (using GP-UCB no-regret alpha, following Brochu 2010 / Srinivas 2012)
            """
            A = 2 * np.log(np.pi**2 / (3*self.alpha_delta))
            tau = A + (self.ndim + 4) * np.log(t - self.nsamples + 1)
            alpha = np.sqrt(self.alpha_nu * tau)
            return alpha

        def get_alpha_constant(t):
            """
            Gets relative importance of exploration/exploitation in acquidsition function
            (constant)
            """
            return self.alpha

        if self.alpha_function == "linear":
            get_alpha = get_alpha_linear
        elif self.alpha_function == "constant":
            get_alpha = get_alpha_constant
        elif self.alpha_function == "no-regret":
            get_alpha = get_alpha_no_regret
        else:
            raise RuntimeError("Invalid value for self.alpha_function")

        def get_beta(t):
            a = self.beta_initial
            b = a - self.beta_final
            return a - b * (t - self.nsamples) / (self.ntotal - self.nsamples)

        mass, nhalos = np.load("halo_density_function.npy")
        density_of_galaxies = 0.0003302568975364238
        density_of_galaxies_error = 1.651284487682119e-05
        from halotools.empirical_models import PrebuiltHodModelFactory

        def compute_ngal(pars, nhalos, mass):
            model = PrebuiltHodModelFactory('zheng07', modulate_with_cenocc=False,
                                            conc_mass_model="dutton_maccio14", mdef="vir")
            model.param_dict['alpha'] = pars[0]
            model.param_dict['logM0'] = pars[1]
            model.param_dict['logM1'] = pars[2]
            model.param_dict['logMmin'] = pars[3]
            model.param_dict['sigma_logM'] = pars[4]

            mean_n = model.mean_occupation_centrals(prim_haloprop=mass) \
                    * (pars[5] + model.mean_occupation_satellites(prim_haloprop=mass))
            ngals = (nhalos * mean_n).sum()

            return ngals

        def density_prior(pars):
            alpha = 3
            L = 1000

            left = density_of_galaxies - 10*density_of_galaxies_error
            right = density_of_galaxies + 10*density_of_galaxies_error
            sigma = density_of_galaxies_error
            x = compute_ngal(pars, nhalos, mass)
            return L * (1 - 1./(1 + np.exp(-alpha * ((x - left) / sigma))) +  1./(1 + np.exp(-alpha * ((x - right) / sigma))))

        def acquisition_function(params, t):
            if (params < limits[:,0]).any() or (params > limits[:,1]).any():
                return np.inf
            varied_parameter_names = [par.name for par in self.pipeline.varied_params]
            par_dict = {name: value for name, value in zip(varied_parameter_names, params)}
            loglike, loglike_var = self.get_emulated_likelihood_and_variance(par_dict)
            loglike_std = np.sqrt(loglike_var)
            return (-1) * (loglike + np.log(1 + get_alpha(t) * loglike_std))

        def initial_conditions(n, p0=None):
            left_limits, right_limits = limits[:,0], limits[:,1]
            if p0 is None:
                norm_ics = np.random.random(self.ndim * n).reshape(n, self.ndim)
                rescaled_ics = left_limits + (right_limits - left_limits) * norm_ics
            else:
                done = False
                k = 2
                while not done:
                    ics = np.random.multivariate_normal(p0, cov=3*self.acquisition_covariance, size=k*n)
                    mask = (ics > left_limits).all(axis=1) & (ics < right_limits).all(axis=1)
                    rescaled_ics = ics[mask]
                    if len(rescaled_ics) < n:
                        k *= 2
                    else:
                        rescaled_ics = rescaled_ics[:n]
                        done = True
            return rescaled_ics

        start_epoch = self.ndone
        final_epoch = min(self.ndone+self.nstep, self.ntotal)
        xtol        = np.sqrt(np.linalg.eigvals(self.acquisition_covariance)).min() * 0.1
        xtol        = max(xtol, min((limits[:,1] - limits[:,0]) * 0.01))

        sampling_points = []
        acquisition_maxima = []

        if os.path.exists(self.sampling_points_fname):
            print("Loading pre-computed sampling points from {}".format(self.sampling_points_fname))
            displaced_points = np.load(self.sampling_points_fname)
            acquisition_points = np.load(self.acquisition_maxima_fname)

            assert displaced_points.ndim       == 2
            assert acquisition_points.ndim     == 2
            assert displaced_points.shape[1]   == self.ndim
            assert acquisition_points.shape[1] == self.ndim
            assert len(displaced_points) == len(acquisition_points)

            for displaced_point, point in zip(displaced_points, acquisition_points):
                if np.isclose(np.atleast_2d(displaced_point), self.emulator.params).all(axis=1).any():
                    continue
                self.emulator.add_observations(displaced_point)
                sampling_points.append(displaced_point)
                acquisition_maxima.append(point)
            start_epoch += len(sampling_points)

        # Finding loglike maximum to sample initial condition close by
        ntries = 10
        results, funs = [], []
        get_alpha, get_alpha_bckp = lambda x: 0, get_alpha
        for _ in range(ntries):
            for ic in initial_conditions(10):
                with np.testing.suppress_warnings() as sup:
                    sup.filter(RuntimeWarning)
                    result = minimize(acquisition_function, x0=ic, args=self.ndone,
                                      method="powell", bounds=limits,
                                      options={"xtol": xtol, "maxfev":1000})
                if not result.success or np.isnan(result.fun):
                    print("WARNING: Failure while optimizing acquisition function. Scipy message: ",
                          file=sys.stderr)
                    print(result, file=sys.stderr)
                if result.success:
                    results.append(result.x)
                    funs.append(result.fun)
            if len(results) > 0:
                break
        if len(results) == 0:
            print("WARNING: Could not find likelihood maximum.",
                  file=sys.stderr)
            p0 = None
        else:
            results = np.array(results)
            funs = np.array(funs)
            p0 = results[np.argmin(funs)]
        get_alpha = get_alpha_bckp

        for epoch in range(start_epoch, final_epoch):
            results, funs = [], []
            ntries = 10
            for _ in range(ntries):
                for ic in initial_conditions(4, p0=p0):
                    with np.testing.suppress_warnings() as sup:
                        sup.filter(RuntimeWarning)
                        result = minimize(acquisition_function, x0=ic, args=epoch,
                                          method="powell", bounds=limits,
                                          options={"xtol": xtol, "maxfev":1000})
                    if not result.success or np.isnan(result.fun):
                        print("WARNING: Failure while optimizing acquisition function. Scipy message: ",
                              file=sys.stderr)
                        print(result, file=sys.stderr)
                    if result.success:
                        results.append(result.x)
                        funs.append(result.fun)
                    else:
                        results.append(np.nan * np.ones_like(ic))
                        funs.append(np.inf)
                if not np.isinf(funs).all():
                    idx = np.argmin(funs)
                    point = results[idx]
                    break
                else:
                    point = None
                    continue

            if point is None:
                raise ValueError("Failed optimization of acquisition function for all initial "
                                 "conditions.")

            delta = limits[:,1] - limits[:,0]
            if (np.isclose(point, limits[:,0], atol=delta*0.05) | np.isclose(point, limits[:,1], atol=delta*0.05)).any():
                print("WARNING: Acquisition point is very close to limits of search region.", file=sys.stdout)
                print("         Maybe you should reduce the value of alpha to prevent that", file=sys.stdout)
                print("         or increase the density of points.", file=sys.stdout)

            print("epoch: ", epoch)
            print("alpha: ", get_alpha(epoch))
            print("acquisition function maximum: ", point)
            while True:
                displaced_point = np.random.multivariate_normal(mean=point,
                                                                cov=get_beta(epoch)*self.acquisition_covariance)
                if (displaced_point > limits[:,0]).all() and (displaced_point < limits[:,1]).all():
                    break
            print("displaced sampling point: ", displaced_point)
            sampling_points.append(displaced_point)
            acquisition_maxima.append(point)
            self.emulator.add_observations(sampling_points[-1])
            if self.add_theo_cov:
                self.setup_emulated_pipeline()

            np.save(self.sampling_points_fname, np.array(sampling_points))
            np.save(self.acquisition_maxima_fname, np.array(acquisition_maxima))

        return np.array(sampling_points)


    def get_emulated_likelihood_and_variance(self, par_dict):
        """
        Computes emulated likelihood and variance
        """
        varied_parameter_names = [par.name for par in self.pipeline.varied_params]
        varied_parameter_section = self.pipeline.varied_params[0].section
        override_dict = dict()
        for key in par_dict:
            override_dict[(varied_parameter_section, key)] = str(par_dict[key])

        par = [par_dict[key] for key in varied_parameter_names]

        with stdout_redirected(to=os.devnull), stdout_redirected(stdout=sys.stderr, to=os.devnull):
            try:
                data = self.emulated_pipeline.run_parameters(par)
            except AttributeError:
                self.setup_emulated_pipeline()
                data = self.emulated_pipeline.run_parameters(par)

        likelihood = data["likelihoods", self.like_name + "_like"]

        loglike_derivative = data[names.data_vector, self.like_name + "_loglike_derivative"]
        emulator_covariance = \
                self.emulator.get_predictive_covariance(self.x, [par_dict[key] for key in par_dict])
        loglike_derivative = loglike_derivative.flatten()[:emulator_covariance.shape[1]]

        likelihood_variance = np.einsum("i,ij,j",
                                        loglike_derivative, emulator_covariance, loglike_derivative)

        return likelihood, likelihood_variance


    def setup_emulated_pipeline(self):
        """
        Loads trained pipeline object to self.emulated_pipeline
        (needed to compute emulated likelihood)
        """
        ini = self.pipeline.options

        if self.add_theo_cov:
            emulator_covariance = \
                    self.emulator.get_predictive_covariance(self.x, self.start_values)

            cov_fname = ini.get(self.covariance_section, self.covariance_key).split(".addedtheocov")[0]

            cov = np.loadtxt(cov_fname)
            cov_shape = cov.shape
            cov = cov.reshape(emulator_covariance.shape)
            cov += emulator_covariance
            new_cov_fname = cov_fname + ".addedtheocov"
            np.savetxt(new_cov_fname, cov.reshape(cov_shape))

        override_dict = {
                         (self.emulated_module, 'is_trained'): "T",
                         ('runtime', 'sampler'): 'test',
                        }
        if isinstance(self.emulated_pipeline, str):
            override_dict[('pipeline', 'modules')] = self.emulated_pipeline
        else:
            override_dict[('pipeline', 'modules')] = ' '.join([x.name for x in self.emulated_pipeline.modules])

        if self.add_theo_cov:
            override_dict[(self.covariance_section, self.covariance_key)] = new_cov_fname

        if self.emulated_pipeline_shortcut != "":
            override_dict[('pipeline', 'shortcut')] = self.emulated_pipeline_shortcut
        for (section, name) in override_dict.keys():
            ini.set(section, name, override_dict[(section, name)])

        with stdout_redirected(to=os.devnull), stdout_redirected(stdout=sys.stderr, to=os.devnull):
            self.emulated_pipeline = LikelihoodPipeline(ini)


    def load_training_file(self):
        """
        Loads self.x, self.y, self.yvar and self.sample_points from
        self.training_file fits file.
        Also checks that pipeline actual configuration is compatible with
        the one used when creating the training file.
        """
        print("Loading training data from training file {}...".format(self.training_file))
        header = fits.getheader(self.training_file)
        module = header["module"]
        nsamples = header["nsamples"]
        nrepeats = header["nrepeats"]
        ndim = header["ndim"]
        try:
            endslice = header["endslice"]
            is_complete = header["is_complete"]
        except KeyError:
            endslice = nsamples
            is_complete = True
        if not is_complete:
            print("Continuing interrupted run at step {}...".format(endslice))
        self.ndone = endslice

        hdul = fits.open(self.training_file)
        if len(hdul) == 6:
            print("Training file produced by older version which didn't save variances.")
            old_version = True              # Previous version didn't save yerr
        elif len(hdul) == 7:
            old_version = False
        else:
            raise RuntimeError("The training file specified is not well formatted.")
        hdul.close()

        #Checks if nsamples and nrepeats agree with pipeline configurations
        if module != self.emulated_module:
            raise RuntimeError("The training file specified is incompatible"
                               "with the pipeline configuration:"
                               "the emulated modules are different.")
        if nsamples != self.nsamples:
            error = "WARNING: emulator trained with {} samples, while pipeline \
                    specifies nsamples = {}".format(nsamples, self.nsamples)
            print(error, file=sys.stderr)
        if nrepeats != self.nrepeats:
            error = "WARNING: emulator trained with {} repeats per sample, \
                    while pipeline specifies nrepeats = {}".format(nrepeats,
                                                                   self.nrepeats)
            print(error, file=sys.stderr)
        if ndim != self.ndim:
            raise RuntimeError("The training file specified is incompatible"
                               "with the pipeline configuration:"
                               "the number of dimensions are different.")


        self.x = np.array(
                          Table.read(self.training_file,
                                     format="fits", hdu=1).to_pandas()
                         ).flatten()
        self.y[:endslice*nrepeats] = np.array(
                                              Table.read(self.training_file,
                                                         format="fits", hdu=2).to_pandas()
                                             )
        self.yvar[:endslice*nrepeats] = 0.
        if not old_version:
            yerr = np.array(
                            Table.read(self.training_file,
                                       format="fits", hdu=6).to_pandas()
                           )
            self.yvar[:endslice*nrepeats] = yerr**2
            self.yerr = np.sqrt(self.yvar)
        nx = self.x.size

        varied_params_table = Table.read(self.training_file,
                                         format="fits", hdu=3)
        loaded_varied_params = np.array(varied_params_table.to_pandas())
        loaded_varied_param_names = varied_params_table.colnames

        try:
            fixed_params_table = Table.read(self.training_file,
                                            format="fits", hdu=4)
            loaded_fixed_params = np.array(
                                           fixed_params_table.to_pandas()
                                          ).flatten()
            loaded_fixed_param_names = fixed_params_table.colnames
        except ValueError:
            loaded_fixed_params = np.array([])
            loaded_fixed_param_names = []

        # Check if fixed parameters on training file match those on pipeline
        for fixed_par_name, fixed_par_value in zip(loaded_fixed_param_names,
                                                   loaded_fixed_params):
            if fixed_par_name not in self.fixed_params_names:
                error = "{} parameter is fixed on training file \
                        but is not on pipeline".format(fixed_par_name)
                raise RuntimeError(error)

            idx = self.fixed_params_names.index(fixed_par_name)
            training_value = fixed_par_value
            pipeline_value = self.fixed_params_values[idx]
            if not np.isclose(pipeline_value, training_value):
                error = "Values for fixed parameter {} differ on\
                        pipeline and on training file.".format(fixed_par_name)
                raise RuntimeError(error)

        # Check if varied parameters on pipeline match those on training
        for varied_par in self.varied_params:
            varied_par_name = str(varied_par)
            if varied_par_name not in loaded_varied_param_names:
                error = "Pipeline varied parameter {} not found \
                        in the training file.".format(varied_par_name)
                raise RuntimeError(error)

        # Check if limits used on training contain limits of pipeline
        trained_limits = np.array(
                                  Table.read(self.training_file,
                                             format="fits", hdu=5).to_pandas()
                                 )
        for varied_par in self.varied_params:
            limit = list(varied_par.limits)
            idx = loaded_varied_param_names.index(str(varied_par))
            trained_limit = trained_limits[idx]
            if trained_limit[0] > limit[0] or trained_limit[1] < limit[1]:
                error = "Trained limits for parameter {} don't include \
                        limits used on pipeline.".format(str(varied_par))
                raise RuntimeError(error)

        # Loads sample_points (aka varied_params)
        n_varied_params = len(loaded_varied_params)
        self.sample_points[:n_varied_params] = loaded_varied_params
        self.varied_params_names = loaded_varied_param_names

        # Take mean over repetitions,
        # calculate covariance matrix and
        # hold the diagonal variances
        if nrepeats > 1:
            print("Computing y values (co)variance/mean by taking the "
                  "(co)variance/mean of the {} repetitions...".format(nrepeats))
            xslice = np.arange(0, endslice)
            yslice = np.arange(0, endslice * nrepeats)
            yslice_mean = np.arange(0, endslice)
            self.compute_means_and_variances(xslice=xslice, yslice=yslice, yslice_mean=yslice_mean)


    def save_training_file(self, endslice):
        """
        Saves the already computed values of x, y and yerr, along with the
        design specification, to self.training_file

        Parameters:
            endslice: int
                number of points already simulated
        """
        print("Saving computed training data (slice ending at {})"
              " to training file {}...".format(endslice, self.training_file))
        header = fits.Header()
        header["module"] = self.emulated_module
        header["nsamples"] = self.nsamples
        header["nrepeats"] = self.nrepeats
        header["ndim"] = self.ndim
        header["endslice"] = endslice

        is_complete = endslice == self.ntotal
        header["is_complete"] = is_complete

        total_samples = self.ntotal * self.nx

        tx = Table(self.x)
        ty = Table(self.y[:endslice*self.nrepeats])
        tyerr = Table(self.yerr[:endslice*self.nrepeats])
        tvaried_params = Table(self.sample_points[:max(endslice, self.nsamples)],
                               names=self.varied_params_names)
        if len(self.fixed_params_values) > 0:
            tfixed_params = Table(np.atleast_2d(self.fixed_params_values),
                                  names=self.fixed_params_names)
            hdufixed_params = fits.table_to_hdu(tfixed_params)
        else:
            hdufixed_params = fits.BinTableHDU()
        tlimits = Table(self.limits)

        hdux = fits.table_to_hdu(tx)
        hduy = fits.table_to_hdu(ty)
        hduyerr = fits.table_to_hdu(tyerr)
        hduvaried_params = fits.table_to_hdu(tvaried_params)
        hdulimits = fits.table_to_hdu(tlimits)
        primary_hdu = fits.PrimaryHDU(header=header)

        hdul = fits.HDUList([primary_hdu,hdux,hduy,
                             hduvaried_params,hdufixed_params,hdulimits,hduyerr])
        hdul.writeto(self.training_file, overwrite=True)


