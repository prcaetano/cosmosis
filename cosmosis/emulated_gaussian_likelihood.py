import sys
import numpy as np
import pickle
import george
import lhsmdu
import os
from astropy.table import Table
from astropy.io import fits
from cosmosis.gaussian_likelihood import GaussianLikelihood
from cosmosis.runtime import parameter
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline
from cosmosis.datablock.cosmosis_py.errors import BlockWrongValueType
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from scipy.linalg import LinAlgError


class EmulatedGaussianLikelihood(GaussianLikelihood):
    emulator_generator = None
    emulator = None

    def __init__(self, options):
        self.block = options.block

        self.training_file = options.get_string("training_file", "training.fits")
        self.pickle_file = options.get_string("pickled_emulator", "pickled_emulator.pkl")
        self.is_trained = options.get_bool("is_trained", False)
        self.nsamples = options.get_int("nsamples")
        self.nrepeats = options.get_int("nrepeats", 1)
        training_file_exists = os.path.isfile(self.training_file)
        pickle_file_exists = os.path.isfile(self.pickle_file)
        self.load_parameters()

        if not self.is_trained:
            self.extract_theory_points = self.nonemulated_extract_theory_points
        elif pickle_file_exists:
            with open(self.pickle_file, "rb") as f:
                self.emulator = pickle.load(f)
        elif training_file_exists:
            self.load_training_file()
            if self.emulator_generator == None:
                raise RuntimeError("Emulator class not specified.")

            # Read noise_level (independent of params, but may depend on x)
            try:
                self.yerr = options.get_double("noise_level", 0.)
            except BlockWrongValueType:
                self.yerr = options.get_string("noise_level")
                yfile = self.yerr
                xerr, yerr = np.loadtxt(yfile, unpack=True)
                interp_err = interp1d(xerr, yerr)
                yerr = interp_err(self.x)
                self.yerr = yerr

            # Summing noise level provided with diagonal of covariance matrix
            self.yerr = np.sqrt(self.yvar + self.yerr**2)

            self.emulator = self.emulator_generator(self.x, self.y, self.yerr, self.varied_params, self.varied_param_names)
            self.emulator.fit()
            with open(self.pickle_file, "wb") as f:
                pickle.dump(self.emulator, f)
        else:
            raise RuntimeError("Training file not provided. Please, train the emulator and specify the training file on the pipeline.")
        super(EmulatedGaussianLikelihood, self).__init__(options)


    def load_parameters(self):
        """
        Loads self.parameters, self.varied_parameters, self.fixed_parameters, self.varied_parameters_names, self.fixed_parameters_names and self.ndim from Datablock.
        """
        values_file = self.block.get("pipeline", "values")
        try:
            priors_file = self.block.get("pipeline", "priors")
        except:
            priors_file = ""

        self.parameters = parameter.Parameter.load_parameters(values_file, priors_file)
        self.varied_parameters = [param for param in self.parameters if param.is_varied()]
        self.fixed_parameters = [param for param in self.parameters if param.is_fixed()]
        self.varied_parameters_names = list(map(str, self.varied_parameters))
        self.fixed_parameters_names = list(map(str, self.fixed_parameters))
        self.ndim = len(self.varied_parameters)


    def load_training_file(self):
        """
        Loads self.x, self.y, self.varied_params, self.fixed_params, self.varied_param_names and self.fixed_param_names from fits training file. Also checks if all fixed parameters on training file are also fixed on pipeline, that all varied parameters on pipeline are also varied parameters on the training file and that limits on training file contain the limits of the pipeline.
        """
        header = fits.getheader(self.training_file)
        module = header["module"]
        nsamples = header["nsamples"]
        nrepeats = header["nrepeats"]
        ndim = header["ndim"]

        #TODO: check module name matchs

        #Checks if nsamples and nrepeats agree with pipeline configurations
        if nsamples != self.nsamples:
            error = "WARNING: emulator trained with {} samples, while pipeline specifies nsamples = {}".format(nsamples, self.nsamples)
            print(error, file=sys.stderr)
        if nrepeats != self.nrepeats:
            error = "WARNING: emulator trained with {} repeats per sample, while pipeline specifies nrepeats = {}".format(nrepeats, self.nrepeats)
            print(error, file=sys.stderr)

        self.x = np.array(Table.read(self.training_file, format="fits", hdu=1).to_pandas()).flatten()
        self.y = np.array(Table.read(self.training_file, format="fits", hdu=2).to_pandas())
        nx = self.x.size

        # Take mean over repetitions, calculate covariance matrix and hold the diagonal variances
        if nrepeats > 1:
            cov = np.array([np.cov(self.y[k:k+nrepeats].T) for k in np.arange(0, nsamples) * nrepeats])
            self.y = np.array([self.y[k:k+nrepeats] for k in np.arange(0, nsamples) * nrepeats]).mean(axis=1)
            self.yvar = np.array([[cov[i,j,j] for j in range(nx)] for i in range(nsamples)])
        else:
            self.yvar = 0

        varied_params_table = Table.read(self.training_file, format="fits", hdu=3)
        self.varied_params = np.array(varied_params_table.to_pandas())
        self.varied_param_names = varied_params_table.colnames

        try:
            fixed_params_table = Table.read(self.training_file, format="fits", hdu=4)
            self.fixed_params = np.array(fixed_params_table.to_pandas()).flatten()
            self.fixed_param_names = fixed_params_table.colnames
        except ValueError:
            self.fixed_params = np.array([])
            self.fixed_param_names = []

        # Check if fixed parameters on training file match those on pipeline
        for fixed_par_name, fixed_par_value in zip(self.fixed_param_names, self.fixed_params):
            if fixed_par_name not in self.fixed_parameters_names:
                error = "{} parameter is fixed on training file but is not on pipeline".format(fixed_par_name)
                raise RuntimeError(error)

            idx = self.fixed_parameters_names.index(fixed_par_name)
            training_value = fixed_par_value
            pipeline_value = self.fixed_parameters[idx].limits[0]
            if not np.isclose(pipeline_value, training_value):
                error = "Values for fixed parameter {} differ on pipeline and on training.".format(fixed_par_name)
                raise RuntimeError(error)

        # Check if varied parameters on pipeline match those on training
        for varied_par in self.varied_parameters:
            varied_par_name = str(varied_par)
            if varied_par_name not in self.varied_param_names:
                error = "Pipeline varied parameter {} not found in the training file.".format(varied_par_name)
                raise RuntimeError(error)

        # Check if limits used on training contain limits of pipeline
        trained_limits = np.array(Table.read(self.training_file, format="fits", hdu=5).to_pandas())
        for varied_par in self.varied_parameters:
            limit = list(varied_par.limits)
            idx = self.varied_param_names.index(str(varied_par))
            trained_limit = trained_limits[idx]
            if trained_limit[0] > limit[0] or trained_limit[1] < limit[1]:
                error = "Trained limits for parameter {} don't include limits used on pipeline.".format(str(varied_par))
                raise RuntimeError(error)


    @classmethod
    def build_module(cls):

        ## Overriding cls.extract_theory_points to use emulator
        old_extract_theory_points = cls.extract_theory_points

        def extract_theory_points(self, block):
            emulator = self.emulator
            params = []
            for param in emulator.param_names:
                section, name = param.split("--")
                try:
                    params.append(block[section, name])
                except:
                    error = "Parameter {} not found in Datablock.".format(param)
                    raise RuntimeError(error)
            return emulator.predict(self.data_x, params, return_var=False, return_cov=False)

        cls.extract_theory_points = extract_theory_points
        cls.nonemulated_extract_theory_points = old_extract_theory_points

        return super(EmulatedGaussianLikelihood, cls).build_module()


class LikelihoodEmulator:
    """
    Subclass this class to describe your emulator. Subclasses should override the fit and predict methods.
    """

    def __init__(self, x, y, yerr, params, param_names):
        """
        Override this constructor to initialize the emulator.

        Parameters:
            x: np.ndarray, shape (nx,)
                x values of function being emulated (could be r or k, for instance)
            y: np.ndarray, shape (nsamples, nx)
                y values of function. Each line contains y(x) for a parameter choice.
            yerr: np.ndarray, shape (nsamples, nx) or (nx,) or float
                gaussian uncertainties of y
            params: np.ndarray, shape (nsamples, ndim)
                values of parameters. Each line corresponds to a sample.
            param_names: list, lenght ndim
                list containing names of the parameters
        """
        try:
            self.nx, = x.shape
        except ValueError:
            raise ValueError("x should be 1d array")
        try:
            self.nsamples, ncols_y = y.shape
        except ValueError:
            raise ValueError("y should be 2d array")
        if ncols_y != self.nx:
            raise ValueError("shape mismatch between x and y.")
        try:
            ncols_params, self.ndim = params.shape
        except ValueError:
            raise ValueError("params should be 2d array")
        if ncols_params != self.nsamples:
            raise ValueError("shape mismatch between y and params.")
        if len(param_names) != self.ndim:
            raise ValueError("mismatch between params shape and lenght of param_names list.")
        if type(yerr) is float:
            yerr = np.ones_like(y) * yerr
        else:
            yerr = np.array(yerr)
            if yerr.ndim == 1:
                if yerr.shape[0] != self.nx:
                    raise ValueError("shape mismatch between yerr and x.")
            elif yerr.ndim == 2:
                if yerr.shape[0] != self.nsamples or yerr.shape[1] != self.nx:
                    raise ValueError("shape mismatch between yerr and x and y.")
            else:
                raise ValueError("wrong number of dimensions for yerr.")

        self.x = x
        self.y = y
        self.yerr = yerr
        self.params = params
        self.param_names = param_names


    def fit(self):
        """
        Override this method to fit the emulator.
        """
        raise NotImplemented


    def predict(self, x, params):
        """
        Override this method to evaluate the emulator on given x and params.

        Parameters:
            x: 1-dim np.ndarray or float
                value(s) to evaluate function on
            params: np.ndarray, shape (ndim,)
                values of parameter to evaluate function on
        """
        raise NotImplemented


class GPEmulator(LikelihoodEmulator):
    kernel = None
    mean = 0.0
    fit_mean = False
    white_noise = None
    fit_white_noise = False
    solver = george.BasicSolver
    initial_conditions = None
    par_range = None
    method = "L-BFGS-B"
    ntries = 1

    def __init__(self, x, y, yerr, params, param_names):
        super(GPEmulator, self).__init__(x, y, yerr, params, param_names)

        if not self.kernel:
            raise RuntimeError("No kernel specified!")
        if (self.ndim + 1) != self.kernel.ndim:
            raise ValueError("kernel and params number of dimensions don't match.")

        if self.white_noise is not None:
            self.gp = george.GP(self.kernel, solver=self.solver, fit_white_noise=self.fit_white_noise,
                                white_noise=self.white_noise, mean=self.mean, fit_mean=self.fit_mean)
        else:
            self.gp = george.GP(self.kernel, solver=self.solver, mean=self.mean, fit_mean=self.fit_mean)

        # Format input data for training
        x_repeated = np.repeat(np.atleast_2d(self.x), self.nsamples, axis=0).reshape(self.nsamples*self.nx,1)
        params_repeated = np.repeat(self.params, self.nx, axis=0)
        self._input = np.c_[x_repeated, params_repeated]
        self._output = self.y.flatten()
        self._output_err = self.yerr.flatten()

        self.gp.compute(self._input, self._output_err)

        if self.par_range is not None:
            ndim, _ = self.par_range.shape
            self.initial_conditions = self.create_initial_conditions(ndim, self.ntries, self.par_range)
        elif self.initial_conditions is None:
            self.initial_conditions = self.gp.get_parameter_vector()
        self.initial_conditions = np.atleast_2d(self.initial_conditions)


    def fit(self):
        results = []
        funs = []
        for initial_condition in self.initial_conditions:
            self.gp.set_parameter_vector(initial_condition)
            try:
                result = minimize(self.neg_ln_like, self.gp.get_parameter_vector(),
                                  jac=self.grad_neg_ln_like, method=self.method)
            except LinAlgError:
                print("WARNING: some initial condition resulted on ill-posed matrixes.", file=sys.stderr)
                continue
            if not result.success or np.isnan(result.fun):
                print("WARNING: some initial conditions failed to converge. Scipy message:", file=sys.stderr)
                print(result)
                continue
            results.append(result)
            funs.append(result.fun)
        try:
            result = results[np.argmin(funs)]
        except ValueError:
            raise ValueError("Minimizer failed to converge for all tried initial conditions.")

        self.gp.set_parameter_vector(result.x)


    def predict(self, x, params, return_var=False, return_cov=False):
        p = np.c_[x, np.repeat(np.atleast_2d(params), len(x), axis=0)]
        ret = self.gp.predict(self._output, p, return_var=return_var, return_cov=return_cov)
        return ret


    @classmethod
    def build_emulator(cls, kernel, initial_conditions=None, par_range=None, ntries=1,
                       white_noise=None, fit_white_noise=False,
                       mean=0., fit_mean=False, solver=None, method="L-BFGS-B"):
        ## TODO Document this better
        """
        Returns class with gaussian process emulator specified and ready to be instantiated.
        """
        if initial_conditions is not None and par_range is not None:
            raise ValueError("You should choose only one between initial_conditions and par_range to specify.")

        ndim = kernel.ndim
        npars = kernel.get_parameter_vector().size
        if fit_white_noise:
            if type(white_noise) == float:
                npars += 1
            else:
                try:
                    npars += white_noise.get_parameter_vector().size
                except AttributeError:
                    raise ValueError("white_noise should be a float or a object implementing george's modelling protocol.")

        if fit_mean:
            if type(mean) == float:
                npars += 1
            else:
                try:
                    npars += mean.get_parameter_vector().size
                except AttributeError:
                    raise ValueError("mean should be a float or a object implementing george's modelling protocol.")

        if par_range is not None:
            par_range = np.array(par_range)
            if par_range.shape != (npars, 2):
                raise ValueError("Shape mismatch between par_range and the kernel.")

        if initial_conditions is not None:
            initial_conditions = np.array(initial_conditions)
            if initial_conditions.shape != (npars,) and initial_conditions.shape != (ntries, npars):
                raise ValueError("Shape mismatch between initial_conditions and the kernel.")

        cls.kernel = kernel
        cls.initial_conditions = initial_conditions
        cls.par_range = par_range
        cls.ntries = ntries
        cls.white_noise = white_noise
        cls.fit_white_noise = fit_white_noise
        cls.mean = mean
        cls.fit_mean = fit_mean
        if solver is not None:
            cls.solver = solver
        cls.method = method

        return cls


    def neg_ln_like(self, p=None):
        if p is not None:
            self.gp.set_parameter_vector(p)
        ll = self.gp.log_likelihood(self._output, quiet=True)
        return -ll


    def grad_neg_ln_like(self, p=None):
        if p is not None:
            self.gp.set_parameter_vector(p)
        grad = self.gp.grad_log_likelihood(self._output, quiet=True)
        return -grad


    @staticmethod
    def create_initial_conditions(ndim, nsamples, limits):
        sample_points = np.random.rand(nsamples, ndim)
        sample_points = (limits[:,1] - limits[:,0]) * sample_points + limits[:,0]
        return sample_points


