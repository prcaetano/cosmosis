#coding: utf-8

u"""Definition of :class:`EmulatedModule` and :class:`GPPCAEmulatedModule`."""

from __future__ import print_function
from builtins import object

import os
import numpy as np
import pickle
from cosmosis.runtime.module import Module
from cosmosis.runtime.module import MODULE_TYPE_SETUP
from cosmosis.runtime.module import MODULE_TYPE_EXECUTE_SIMPLE
from cosmosis.runtime.module import MODULE_TYPE_EXECUTE_CONFIG
from cosmosis.runtime.module import MODULE_TYPE_CLEANUP
from cosmosis.runtime.module import MODULE_LANG_PYTHON

from cosmosis.runtime.emulator import GPPCALogEmulator, GPLogEmulator, GPPCAEmulator, GPEmulator

import george
from george.kernels import ExpSquaredKernel, Matern32Kernel, Matern52Kernel, EmptyKernel, PolynomialKernel, RationalQuadraticKernel


class EmulatedModule(object):
    u"""Base class for creation of emulated modules, based on existent
    (python or not) modules.

    This class handles the interfacing between modules and emulators,
    allowing users to create emulated modules by subclassing.

    To create a new EmulatedModule, the user should proceed in a similar
    way as when creating class-based python Modules, but inheriting this
    class instead. The subclass should specify
        * the emulator_generator attribute, tipically by calling the
        build_emulator method of an class implementing the Emulator interface
        * the x_section, x_name, y_section and y_name attributes,
        describing the section and key of the dataframe to look for the
        x and y values of the function to emulated
        * Optionally, the yerr_name attribute, describing the key to use
        to save the calculated error of the emulated y value
        (the section is always y_section)
        * Optionally, the ycov_name attribute, describing the key to use
        to save the full calculated covariance matrix of y values
        * the build_data method, which loads the data_x and data_y
        attributes containing the observed data.

    This class handles the loading of trained emulators and the creation
    of the /setup/, /execute/ and /cleanup/ functions.

    Different behaviour occur depending on the flag is_trained, loaded
    from the module section on the pipeline file. If true, this module
    doesn't load the emulator object and instead exposes the /setup/,
    /execute/ and /cleanup/ functions of the original non-emulated module.
    Otherwise, it exposes the /setup/, /execute/ and /cleanup/ functions
    created based on the emulator.
    """
    emulator_generator = None
    emulator = None
    x_section = None
    y_section = None
    x_name = None
    y_name = None
    yerr_name = None
    ycov_name = None


    def __init__(self, options, name):
        """
        If is_trained parameter on the ini configuration file is True,
        loads trained_emulator from the file specified under the
        trained_emulator key and creates execute_function
        that runs the emulator and stores the result on name + "_theory",
        under data_vector section on the datablock.

        Otherwise, loads setup, execute and cleanup from nonemulated_file,
        and runs its setup.

        When subclassing, you should override this so that the attribute
        emulator_generator is specified, and this method is called
        afterwards (via super).
        """
        self.mod_name = name
        self.block = options

        self.original_module = options.get_string(name, "nonemulated_file")
        self.pickle_file = options.get_string(name, "emulator_file", "training.pkl")
        self.is_trained = options.get_bool(name, "is_trained", False)
        pickle_file_exists = os.path.isfile(self.pickle_file)

        self.predict_cov = options.get_bool(name, "predict_cov", True)
        self.predict_err = options.get_bool(name, "predict_err", True)

        self.data_x, self.data_y = self.build_data()

        if not self.is_trained:
            self.original_library, self.original_language = \
                    Module.load_library(self.original_module)
            self.is_python = (self.original_language == MODULE_LANG_PYTHON)

            self.setup_function = \
                    Module.load_function(self.original_library,
                                         "setup", MODULE_TYPE_SETUP)
            self.cleanup_function = \
                    Module.load_function(self.original_library,
                                         "cleanup", MODULE_TYPE_CLEANUP)

            if not self.is_python:
                options = options._ptr
            self.setup_data = self.setup_function(options)
            if self.setup_data != None:
                self.execute_function = \
                        Module.load_function(self.original_library,
                                             "execute", MODULE_TYPE_EXECUTE_CONFIG)
            else:
                self.execute_function = \
                        Module.load_function(self.original_library,
                                             "execute", MODULE_TYPE_EXECUTE_SIMPLE)
        else:
            if pickle_file_exists:
                with open(self.pickle_file, "rb") as f:
                    self.emulator = pickle.load(f)
            else:
                raise RuntimeError("Trained emulator pickle file not found."
                                   "Please train the emulator"
                                   "by using the emulatortrainer sampler.")

            def execute_function(block):
                emulator = self.emulator
                params = []

                for param in emulator.param_names:
                    section, name = param.split("--")
                    try:
                        params.append(block[section, name])
                    except:
                        error = "Parameter {} not found in Datablock.".format(param)
                        raise RuntimeError(error)

                if (self.ycov_name != None and self.predict_cov and
                    self.yerr_name != None and self.predict_err):
                    block[self.y_section, self.y_name], variance, covariance = \
                            emulator.predict(self.data_x, params,
                                             return_var=True, return_cov=True)
                    block[self.y_section, self.yerr_name] = np.sqrt(variance)
                    block[self.y_section, self.ycov_name] = covariance

                if self.ycov_name != None and self.predict_cov:
                    block[self.y_section, self.y_name], covariance = \
                            emulator.predict(self.data_x, params,
                                             return_var=False, return_cov=True)

                    block[self.y_section, self.ycov_name] = covariance

                elif self.yerr_name != None and self.predict_err:
                    block[self.y_section, self.y_name], variance = \
                            emulator.predict(self.data_x, params,
                                             return_var=True, return_cov=False)

                    block[self.y_section, self.yerr_name] = np.sqrt(variance)

                else:
                    block[self.y_section, self.y_name] = \
                            emulator.predict(self.data_x, params,
                                             return_var=False, return_cov=False)
                    block[self.y_section, self.yerr_name] = 0
                    block[self.y_section, self.ycov_name] = 0

                block[self.x_section, self.x_name] = self.data_x
                return 0

            self.execute_function = execute_function
            self.setup_data = None
            self.is_python = True


    def execute(self, block):
        """
        Calls the execute function defined on instantiation.
        """
        if not self.is_python:
            block = block._ptr
        if self.setup_data != None:
            return self.execute_function(block, self.setup_data)
        else:
            return self.execute_function(block)


    def cleanup(self):
        """
        Calls the cleanup function defined on instantiation.
        """
        try:
            if self.setup_data != None:
                return self.cleanup_function(self.setup_data)
            else:
                return self.cleanup_function()
        except AttributeError:
            return 0


    def build_data(self):
        """
        Returns data_x and data_y using information on self
        already loaded on initialization (you need to override this method.)

        It might be useful to access the datablock, which is at the block
        attribute of this class.
        """
        raise NotImplementedError("You should override the build_data"
                                  "method of your module so that it knows"
                                  "how to load the observed data.")


class GPPCAEmulatedModule(EmulatedModule):
    u"""Class implementing basic boilerplate code to implement GP emulated
    modules, based on existing cosmosis modules.

    This class reads from the module configuration files the parameters
        * emulator_parameters_logmin, emulator_parameters_logmax
            describing log range of initial conditions used on training
            the gaussian processes
        * ntrainingtries
            number of different initial conditions to try during training
        * npca
            number of PCA components to use
        * nparameters
            number of parameters the model expects
        * fit_white_noise, fit_mean
            whether GP should fit for the mean and white noise or not
        * kernel
            string representing GP kernel, containing keywords
            'linear', 'exp_squared', 'matern32' and the sum symbol
            (e.g., 'exp_squared + matern32' builds a kernel corresponding
            to sum of ExpSquaredKernel and Matern32Kernel, check george docs)
        * do_pca, do_log
            whether to preprocess data appling PCA and log transformations
        * weight_pca
            whether to use weights for PCA
        * x_as_index
            whether to create independent emulator for each x coordinate, or
            to treat it as another parameter
        * self.y_data_fname (on section sample_files)
            name of file used by build_data (optional)

    Besides that, subclasses need to specify x_section, y_section, x_name, y_name
    yerr_name and ycov_name, describing the expected location of data and theory
    vectors on datablock. A basic build_data function is implemented here,
    assuming data in format compatible with np.loadtxt, but that might also need
    overloading.
    """
    x_section     = None
    y_section     = None
    x_name        = None
    y_name        = None
    yerr_name     = None
    ycov_name     = None
    y_data_fname  = None


    def __init__(self, my_config, my_name):
        self.block = my_config

        a = my_config.get_int(my_name, "emulator_parameters_logmin", -6)
        b = my_config.get_int(my_name, "emulator_parameters_logmax", +6)

        ntries             = my_config.get_int(my_name, "ntrainingtries", 12)
        npca               = my_config.get_int(my_name, "npca", 5)
        n_varied_pars      = my_config.get_int(my_name, "nparameters", 4)
        fit_white_noise    = my_config.get_bool(my_name, "fit_white_noise", True)
        fit_mean           = my_config.get_bool(my_name, "fit_mean", False)
        kernel_spec_string = my_config.get_string(my_name, "kernel", "exp_squared + matern32")

        do_pca                           = my_config.get_bool(my_name, "do_pca", True)
        do_log                           = my_config.get_bool(my_name, "do_log", True)
        weight_pca                       = my_config.get_bool(my_name, "weight_pca", False)
        x_as_index                       = my_config.get_bool(my_name, "x_as_index", True)
        diagonalize_parameter_covariance = my_config.get_bool(my_name,
                                                              "diagonalize_parameter_covariance",
                                                              False)
        if weight_pca:
            pca_implementation = "wpca"
        else:
            pca_implementation = "pca"

        if diagonalize_parameter_covariance:
            parameter_covariance_fname = my_config.get_string(my_name, "parameter_covariance")
            parameter_covariance = np.loadtxt(parameter_covariance_fname)
            N = int(np.sqrt(parameter_covariance.size))
            parameter_covariance = parameter_covariance.reshape(N, N)
        else:
            parameter_covariance = None

        try:
            self.data_file = my_config.get_string("sample_files", self.y_data_fname, "")
        except:
            self.data_file = None

        # Building emulation class
        kernel_parcels = kernel_spec_string.replace(" ", "").lower().split("+")
        n_varied_pars += int(not x_as_index)
        kernel = EmptyKernel(ndim=n_varied_pars, axes=np.arange(n_varied_pars))
        npars = int(fit_white_noise) + int(fit_mean)

        for parcel in kernel_parcels:
            factors = parcel.split("*")
            kernel_parcel = 1.
            npars += 1
            for factor in factors:
                if "(" in factor or ")" in factor:
                    assert factor.count("(") == 1 and factor.count(")") == 1
                    components = factor.split("(")[1].split(")")[0].split(",")
                    axes = np.array(list(map(lambda x: int(x.replace("x", "")), components)))
                    assert (axes >= 1).all() and (axes <= n_varied_pars).all()
                    axes -= 1
                else:
                    axes = np.arange(n_varied_pars)
                metric=np.ones_like(axes)

                if "changepoint" in factor:
                    from george.kernels import ExpSquaredCPExpSquaredKernel

                    assert factor.count("[") == 1 and factor.count("]") == 1
                    assert len(axes) == 1
                    npars += 6

                    k1, k2 = factor.split("[")[1].split("]")[0].split(",")
                    if k1 == "exp_squared" and k2 == "exp_squared":
                        kernel_parcel *= ExpSquaredCPExpSquaredKernel(location=0., log_width=0.5,
                                                                      log_scale_left=0.,
                                                                      log_sigma_left=0.,
                                                                      log_scale_right=0.8,
                                                                      log_sigma_right=0.,
                                                                      ndim=n_varied_pars, axes=axes)
                    else:
                        print("Kernels not recognized or not implemented for use "
                              "with changepoint kernel.", file=sys.stderr)
                        raise
                else:
                    if "exp_squared" in factor:
                        npars += len(axes)
                        kernel_parcel *= ExpSquaredKernel(metric=metric,
                                                          ndim=n_varied_pars,
                                                          axes=axes)
                    if "matern32" in factor:
                        npars += len(axes)
                        kernel_parcel *= Matern32Kernel(metric=0.1*metric,
                                                        ndim=n_varied_pars,
                                                        axes=axes)
                    if "matern52" in factor:
                        npars += len(axes)
                        kernel_parcel *= Matern52Kernel(metric=0.1*metric,
                                                        ndim=n_varied_pars,
                                                        axes=axes)
                    if "linear" in factor:
                        npars += 1
                        k = PolynomialKernel(order=1, ndim=n_varied_pars,
                                             axes=axes, log_sigma2=1.)
                        #for par in k.get_parameter_names():
                        #    if "log_sigma2" in par:
                        #        k.freeze_parameter(par)
                        kernel_parcel *= k
                    if "rational_quadratic" in factor:
                        npars += len(axes) + 1
                        kernel_parcel *= RationalQuadraticKernel(metric=metric,
                                                                 log_alpha=1.,
                                                                 ndim=n_varied_pars,
                                                                 axes=axes)
                kernel += kernel_parcel

        par_range = [[a, b] for _ in range(npars)]

        solver = george.BasicSolver
        white_noise = 1e0
        mean_model = 0.

        if do_log:
            if do_pca:
                self.emulator_generator = GPPCALogEmulator.build_emulator(kernel,
                                                                          par_range=par_range,
                                                                          ntries=ntries,
                                                                          white_noise=white_noise,
                                                                          fit_white_noise=fit_white_noise,
                                                                          mean=mean_model,
                                                                          fit_mean=fit_mean,
                                                                          solver=solver,
                                                                          npca=npca,
                                                                          logx=False,
                                                                          logy=True,
                                                                          pca_implementation=pca_implementation,
                                                                          parameter_covariance=parameter_covariance,
                                                                         )
            else:
                self.emulator_generator = GPLogEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       logx=False,
                                                                       logy=True,
                                                                       x_as_index=x_as_index,
                                                                       parameter_covariance=parameter_covariance,
                                                                      )
        else:
            if do_pca:
                self.emulator_generator = GPPCAEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       npca=npca,
                                                                       logx=False,
                                                                       logy=False,
                                                                       pca_implementation=pca_implementation,
                                                                       parameter_covariance=parameter_covariance,
                                                                      )
            else:
                self.emulator_generator = GPLogEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       x_as_index=x_as_index,
                                                                       parameter_covariance=parameter_covariance,
                                                                       logx=False,
                                                                       logy=False,
                                                                      )


        super(GPPCAEmulatedModule, self).__init__(my_config, my_name)


    def build_data(self):
        """
        Loads self.data_x, self.data_y and self.data_y_err from self.data_file,
        assuming a format compatible with np.loadtxt.
        """
        self.data_x, self.data_y, self.data_y_err = np.loadtxt(self.data_file, unpack=True)
        return self.data_x, self.data_y

