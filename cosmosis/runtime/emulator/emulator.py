#coding: utf-8

u"""Definition of :class:`Emulator`, :class:`GPEmulator` and :class:`GPLogEmulator`."""

from __future__ import print_function
from builtins import object

import sys
import numpy as np
import pickle
import george
import lhsmdu
import os
from astropy.table import Table
from astropy.io import fits
from cosmosis.gaussian_likelihood import GaussianLikelihood
from cosmosis.runtime import parameter
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline
from cosmosis.datablock.cosmosis_py.errors import BlockWrongValueType
from cosmosis.runtime.module import Module
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section
from cosmosis.runtime.declare import declare_module
from cosmosis.runtime.module import Module, MODULE_TYPE_SETUP, MODULE_TYPE_EXECUTE_SIMPLE, MODULE_TYPE_EXECUTE_CONFIG, MODULE_TYPE_CLEANUP, MODULE_LANG_PYTHON
from george.kernels import ExpSquaredKernel
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from scipy.linalg import LinAlgError
from sklearn.decomposition import PCA
from sklearn import preprocessing
from itertools import repeat


class Emulator(object):
    """
    Subclass this class to describe your emulator. Subclasses should override the fit and predict methods.
    """
    initial_conditions = None


    def __init__(self, x, y, yerr, params, param_names, seed=42):
        """
        Initializes the emulator.

        Note that to build the emulator (that is, describe the kernel and
        initial conditions to use) you should instead call the build_emulator
        method, which return an adapted version of this class to be
        instantiated by the emulatortrainer sampler during training.

        Parameters:
            x: np.ndarray, shape (nx,)
                x values of function being emulated (could be r or k, for instance)
            y: np.ndarray, shape (nsamples, nx)
                y values of function. Each line contains y(x) for a parameter choice.
            yerr: np.ndarray, shape (nsamples, nx) or (nx,) or float
                gaussian uncertainties of y
            params: np.ndarray, shape (nsamples, ndim)
                values of parameters. Each line corresponds to a sample.
            param_names: list, lenght ndim
                list containing names of the parameters
            seed: int
                seed for random number generator involved in training/generating initial conditions. Defaults to 42.
        """
        try:
            self.nx, = x.shape
        except ValueError:
            raise ValueError("x should be 1d array")
        try:
            self.nsamples, ncols_y = y.shape
        except ValueError:
            raise ValueError("y should be 2d array")
        if ncols_y != self.nx:
            raise ValueError("shape mismatch between x and y.")
        try:
            ncols_params, self.ndim = params.shape
        except ValueError:
            raise ValueError("params should be 2d array")
        if ncols_params != self.nsamples:
            raise ValueError("shape mismatch between y and params.")
        if len(param_names) != self.ndim:
            raise ValueError("mismatch between params shape and lenght of param_names list.")
        if type(yerr) is float:
            yerr = np.ones_like(y) * yerr
        else:
            yerr = np.array(yerr)
            if yerr.ndim == 0:
                pass
            elif yerr.ndim == 1:
                if yerr.shape[0] != self.nx:
                    raise ValueError("shape mismatch between yerr and x.")
            elif yerr.ndim == 2:
                if yerr.shape[0] != self.nsamples or yerr.shape[1] != self.nx:
                    raise ValueError("shape mismatch between yerr and x and y.")
            else:
                raise ValueError("wrong number of dimensions for yerr.")
        if not isinstance(seed, int):
            raise ValueError("seed should be an integer. Try 42 instead.")

        self.x = x
        self.y = y
        self.yerr = yerr
        self.params = params
        self.param_names = param_names
        self.seed = seed


    def set_parameters(self, emulator_parameters):
        """
        Override this method to set the emulator parameters
        to the given values.
        """
        raise NotImplemented


    def get_parameters(self):
        """
        Override this method to get the emulator (hyper)parameters
        """
        raise NotImplemented


    def fit(self, initial_conditions=None, suppress_errors=False):
        """
        Override this method to fit the emulator.

        Returns:
            best_fit_emulator_parameters, best_fit_score
        """
        raise NotImplemented


    def predict(self, x, params):
        """
        Override this method to evaluate the emulator on given x and params.

        Parameters:
            x: 1-dim np.ndarray or float
                value(s) to evaluate function on
            params: np.ndarray, shape (ndim,)
                values of parameter to evaluate function on
        """
        raise NotImplemented


class GPEmulator(Emulator):
    kernel = None
    mean = 0.0
    fit_mean = False
    white_noise = None
    fit_white_noise = False
    solver = george.BasicSolver
    initial_conditions = None
    par_range = None
    ntries = 1
    x_as_index = False
    standardize_input = True
    standardize_mean_output = True
    standardize_std_output = True
    parameter_covariance = None
    diagonalize_parameter_covariance = False

    def __init__(self, x, y, yerr, params, param_names, seed=42):

        super(GPEmulator, self).__init__(x, y, yerr, params, param_names, seed)

        if not self.kernel:
            raise RuntimeError("No kernel specified!")
        if self.ndim + int(not self.x_as_index) != self.kernel.ndim:
            print("self.ndim = {}".format(self.ndim), file=sys.stderr)
            print("self.x.shape = {}".format(self.x.shape), file=sys.stderr)
            print("self.kernel.ndim = {}".format(self.kernel.ndim), file=sys.stderr)
            raise ValueError("kernel and params number of dimensions don't match.")

        if self.x_as_index:
            #just to initialize objects possibly needed by self.transform_input_x
            _ = self.transform_input_y_and_yerr(y, yerr)

            self.n_of_emulators = len(self.transform_input_x(x))
        else:
            self.n_of_emulators = 1

        ## Instantiating GP objects
        from copy import deepcopy
        self.gps = []

        for _ in range(self.n_of_emulators):
            kernel = deepcopy(self.kernel)

            if self.white_noise is not None:
                self.gps.append(george.GP(kernel,
                                          solver=self.solver,
                                          fit_white_noise=self.fit_white_noise,
                                          white_noise=self.white_noise,
                                          mean=self.mean,
                                          fit_mean=self.fit_mean))
            else:
                self.gps.append(george.GP(kernel,
                                          solver=self.solver,
                                          mean=self.mean,
                                          fit_mean=self.fit_mean))


        ## Formatting data and preparing GP objects
        self.setup_gp(self.params, self.x, self.y, self.yerr)

        ## Setting up initial conditions for fitting
        if self.par_range is not None:
            ndim, _ = self.par_range.shape
            self.initial_conditions = self.create_initial_conditions(ndim, self.ntries, self.par_range, self.seed)
        elif self.initial_conditions is None:
            self.initial_conditions = self.gp.get_parameter_vector()
        self.initial_conditions = np.atleast_2d(self.initial_conditions)


    def transform_input_x(self, x):
        return x


    def transform_input_y_and_yerr(self, old_y, old_yerr):
        try:
            new_y = self.output_scaler.transform(old_y)
            new_yerr = old_yerr / self.output_scaler.scale_
        except AttributeError:
            self.output_scaler = preprocessing.StandardScaler(with_mean=self.standardize_mean_output,
                                                              with_std=self.standardize_std_output).fit(old_y)
            new_y = self.output_scaler.transform(old_y)
            new_yerr = old_yerr / self.output_scaler.scale_

        return new_y, new_yerr


    def transform_params(self, x, params):
        if self.diagonalize_parameter_covariance:
            try:
                params = np.matmul(self.U, params.T).T
            except AttributeError:
                _, v = np.linalg.eig(self.parameter_covariance)
                self.U = v.T
                params = np.matmul(self.U, params.T).T
        if not self.x_as_index:
            nsamples = params.shape[0]
            nx = len(x)
            x_repeated = np.repeat(np.atleast_2d(x),
                                   nsamples, axis=0).reshape(-1, 1)
            params_repeated = np.repeat(params, nx, axis=0)
            params = np.c_[x_repeated, params_repeated]

        if self.standardize_input:
            try:
                params = self.input_scaler.transform(params)
            except AttributeError:
                self.input_scaler = preprocessing.MinMaxScaler().fit(params)
                params = self.input_scaler.transform(params)
        return params


    def inverse_transform_output(self, output, covariance):
        transformed_output = self.output_scaler.inverse_transform(output.flatten())

        if self.x_as_index:
            transformed_covariance = np.diag(covariance.flatten())
        else:
            transformed_covariance = covariance[0]

        if self.output_scaler.scale_ is not None:
            scaling = np.matmul(self.output_scaler.scale_.reshape(-1, 1),
                                self.output_scaler.scale_.reshape(1, -1))
            transformed_covariance *= scaling

        return transformed_output, transformed_covariance


    def setup_gp(self, params, x, y, yerr):

        y, yerr = self.transform_input_y_and_yerr(y, yerr)
        x = self.transform_input_x(x)
        params = self.transform_params(x, params)

        nsamples = params.shape[0]

        # Format and preprocess data
        if self.x_as_index:
            self._output = []
            self._output_err = []
            self._input = []

            for y_, yerr_ in zip(y.T, yerr.T):
                self._output.append(y_)
                self._output_err.append(yerr_)
                self._input.append(params)

            self._input = np.array(self._input)
            self._output = np.array(self._output)
            self._output_err = np.array(self._output_err)
        else:
            self._input = np.array([params])
            self._output = np.atleast_2d(y.flatten())
            self._output_err = np.atleast_2d(yerr.flatten())

        if self.x_as_index:
            for gp, yerr in zip(self.gps, yerr.T):
                gp.compute(params, yerr)
        else:
            self.gps[0].compute(self._input[0], self._output_err[0])


    def add_observations(self, new_params):
        """
        Adds simulated observations at new_params, updating the emulator using
        augmented training data by including as observations the predicted
        values on new_params under the old model.

        Parameters:
            new_params: np.ndarray, shape (n_new_params, ndim)
                parameters to use to augment the training data
        """
        new_params = np.atleast_2d(new_params)

        ## Drop parameters already included
        mask_old_params = []
        for new_param in new_params:
            mask_old_params.append(np.isclose(new_param, self.params).all(axis=1).any())
        mask_old_params = np.array(mask_old_params)
        new_params = new_params[~mask_old_params]

        ## Simulate values of y and yerr at new_params
        new_y = np.array([self.predict(self.x, new_param, return_var=False, return_cov=False)
                          for new_param in new_params])
        new_yerr = np.zeros_like(new_y)

        ## Prepare data and recompute the GP objects
        params = np.append(self.params, new_params, axis=0)
        x = self.x
        y = np.append(self.y, new_y, axis=0)
        yerr = np.append(self.yerr, new_yerr, axis=0)
        self.setup_gp(params, x, y, yerr)


    def set_parameters(self, parameters):
        parameters = np.atleast_2d(parameters)
        for gp, pars in zip(self.gps, parameters):
            if not np.isnan(pars).any():
                gp.set_parameter_vector(pars)


    def get_parameters(self):
        return np.array([gp.get_parameter_vector() for gp in self.gps])


    def fit(self, initial_condition=None, suppress_errors=False):

        max_log_likes = []
        parameters = []

        initial_conditions = np.array(initial_condition)

        if (initial_conditions == None).any():
            initial_conditions = self.initial_conditions
        else:
            initial_conditions = np.atleast_2d(initial_condition)

        print("Fit call initial_conditions: ", initial_conditions)

        for gp, y in zip(self.gps, np.atleast_2d(self._output)):
            results = []
            funs = []

            for initial_condition in initial_conditions:
                gp.set_parameter_vector(initial_condition)
                try:
                    result = minimize(lambda p: self.neg_ln_like(p, gp=gp, y=y),
                                      gp.get_parameter_vector(),
                                      jac=lambda p: self.grad_neg_ln_like(p, gp=gp, y=y),
                                      method="L-BFGS-B",
                                      options={"ftol": 1e-3})
                except LinAlgError:
                    print("WARNING: some initial condition resulted on ill-posed matrixes.", file=sys.stderr)
                    continue
                if not result.success or np.isnan(result.fun):
                    print("WARNING: some initial conditions failed to converge. Scipy message:", file=sys.stderr)
                    print(result, file=sys.stderr)
                    continue
                if result.success:
                    results.append(result.x)
                    funs.append(result.fun)
                else:
                    results.append(np.nan*np.ones_like(gp.get_parameter_vector()))
                    funs.append(np.inf)

            results = np.array(results)
            funs = np.array(funs)

            try:
                idx = np.argmin(funs)
                parameters.append(results[idx])
                max_log_likes.append(-funs[idx])
            except ValueError:
                if not suppress_errors:
                    raise ValueError("Minimizer failed to converge for all tried initial conditions.")
                else:
                    max_log_likes.append(-np.inf)
                    parameters.append(np.nan * gp.get_parameter_vector())

        parameters = np.array(parameters)
        max_log_likes = np.array(max_log_likes)
        self.set_parameters(parameters)

        print("Result: {}, Fun: {}".format(parameters, max_log_likes))
        return [parameters, max_log_likes]


    def predict(self, x, params, return_var=False, return_cov=False):
        """
        Returns the emulated values at positions x and parameters params.

        If asked, also returns the covariance matrix and/or the variance.

        Parameters:
            x: 1-dim np.ndarray or float
                value(s) to evaluate function on
            params: np.ndarray, shape (ndim,)
                values of parameter to evaluate function on
            return_var: bool
                whether to return the variance
            return_cov: bool
                whether to return the covariance matrix

        Returns:
            if return_cov and return_var:
                return outputs, variances, covariances
            elif return_cov:
                return outputs, covariances
            elif return_var:
                return outputs, variances
            else:
                return outputs
        """

        if self.x_as_index:
            if not np.isclose(self.x, x, rtol=1e-2).all():
                raise RuntimeError("x array passed to emulator.predict function"
                                   " should be equal to data_x attribute if x"
                                   " variable is treated as index"
                                   " (x_as_index is True).")

        x = self.transform_input_x(x)
        p = self.transform_params(x, np.atleast_2d(params))

        outputs = []
        covariances = []
        for gp, y in zip(self.gps, np.atleast_2d(self._output)):
            output, cov = gp.predict(y, p, return_cov=True, return_var=False)
            outputs.append(output)
            covariances.append(cov)

        outputs = np.array(outputs)
        covariances = np.array(covariances)

        transformed_outputs, transformed_covariances = self.inverse_transform_output(outputs,
                                                                                     covariances)
        transformed_variances = np.diagonal(transformed_covariances)

        if return_cov and return_var:
            return transformed_outputs, transformed_variances, transformed_covariances
        elif return_cov:
            return transformed_outputs, transformed_covariances
        elif return_var:
            return transformed_outputs, transformed_variances
        else:
            return transformed_outputs


    def get_predictive_covariance(self, x, params):
        """
        Returns the covariance of the emulator predictions for parameters
        params at positions x.

        Parameters:
            x: 1-dim np.ndarray or float
                value(s) to evaluate function on
            params: np.ndarray, shape (ndim,)
                values of parameter to evaluate function on

        Returns:
            covariances
        """
        _, covariances = self.predict(x, params, return_var=False, return_cov=True)
        return covariances


    @classmethod
    def build_emulator(cls, kernel, initial_conditions=None, par_range=None, ntries=1,
                       white_noise=None, fit_white_noise=False,
                       mean=0., fit_mean=False, solver=None,
                       x_as_index=False, standardize_input=True,
                       standardize_mean_output=True, standardize_std_output=True,
                       parameter_covariance=None,
                       **kwargs):
        """
        Specifies gaussian process emulation details and returns class ready to be instantiated.

        By calling this you are able to specify the kernel, initial conditions and other relevant
        information needed to carry the emulation. You should never actually instantiate this
        class (or the modified version returned by this method). Instead you can use the
        returned class as the emulator_generator attribute of a EmulatedModule subclass,
        and both that class and the emulatortrainer sampler will do the work under the hood.

        You can get a working emulator by specifying only the kernel and one of
        initial_conditions and par_range.

        Parameters:
            kernel:
                a george kernel object with dimensions matched the ones of the parameter space
                you wish to emulate
            initial_conditions: np.ndarray, shaped like (npars, ntries) or (npars,), or None
                array describing the initial conditions to use during training
            par_range: np.ndarray, shaped like (npars, 2), or None
                array describing the limits of the parameter space region intented to be
                emulated. If initial_conditions is absent, it is used to randomly sample
                initial conditions.
            ntries: int
                number of different initial conditions to use when searching the maximum
                likelihood hyperparameters
            white_noise:
                object describing white noise modelling, compatible with george implementation
                (cf. george documentation)
            fit_white_noise: bool
                whether to fit the parameters describing white noise or not.
            mean:
                object describing mean of the gaussian process.
            fit_mean: bool
                whether to fit the parameters describing the mean.
            solver:
                george solver to use for matrix inversions. Defaults to BasicSolver.
            x_as_index: bool
                if True, doesn't treat the x variable of the function being emulated as
                an independent dimension, and instead train a different gaussian process
                for each value of the variable.
            standardize_input: bool
                if True (default), standardize parameters range to lie between 0 and 1
            standardize_mean_output: bool
                if True (default), standardize output by subtracting the mean of training ys
            standardize_std_output: bool
                if True (default), standardize output by dividing by the std of training ys
            parameter_covariance: np.ndarray or None
                if not None, covariance to be diagonalized to transform input parameters
        """
        if initial_conditions is not None and par_range is not None:
            raise ValueError("You should choose only one between initial_conditions and par_range to specify.")

        ndim = kernel.ndim
        npars = kernel.get_parameter_vector().size
        if fit_white_noise:
            if type(white_noise) == float:
                npars += 1
            else:
                try:
                    npars += white_noise.get_parameter_vector().size
                except AttributeError:
                    raise ValueError("white_noise should be a float or a object implementing george's modelling protocol.")

        if fit_mean:
            if type(mean) == float:
                npars += 1
            else:
                try:
                    npars += mean.get_parameter_vector().size
                except AttributeError:
                    raise ValueError("mean should be a float or a object implementing george's modelling protocol.")

        if par_range is not None:
            par_range = np.array(par_range)
            if par_range.shape != (npars, 2):
                raise ValueError("Shape mismatch between par_range and the kernel.")

        if initial_conditions is not None:
            initial_conditions = np.array(initial_conditions)
            if initial_conditions.shape != (npars,) and initial_conditions.shape != (ntries, npars):
                raise ValueError("Shape mismatch between initial_conditions and the kernel.")

        cls.kernel = kernel
        cls.initial_conditions = initial_conditions
        cls.par_range = par_range
        cls.ntries = ntries
        cls.white_noise = white_noise
        cls.fit_white_noise = fit_white_noise
        cls.mean = mean
        cls.fit_mean = fit_mean
        if solver is not None:
            cls.solver = solver
        cls.x_as_index = x_as_index
        cls.standardize_input = standardize_input
        cls.standardize_mean_output = standardize_mean_output
        cls.standardize_std_output = standardize_std_output
        cls.parameter_covariance = parameter_covariance
        cls.diagonalize_parameter_covariance = parameter_covariance is not None

        return cls


    def neg_ln_like(self, p=None, gp=None, y=None):
        if gp is None:
            gp = self.gps[0]

        if y is None:
            y = self._output[0]

        if p is not None:
            gp.set_parameter_vector(p)
        ll = gp.log_likelihood(y, quiet=True)
        return -ll


    def grad_neg_ln_like(self, p=None, gp=None, y=None):
        if gp is None:
            gp = self.gps[0]

        if y is None:
            y = self._output[0]

        if p is not None:
            gp.set_parameter_vector(p)
        grad = gp.grad_log_likelihood(y, quiet=True)
        return -grad


    @staticmethod
    def create_initial_conditions(ndim, nsamples, limits, seed):
        np.random.seed(seed)
        sample_points = np.random.rand(nsamples, ndim)
        sample_points = (limits[:,1] - limits[:,0]) * sample_points + limits[:,0]
        return sample_points



class GPLogEmulator(GPEmulator):
    """
    Gaussian Process emulator that performs emulation on log space.

    This class is intented to emulate functions y(x, pars) by first
    changing variables to log space, that is, it actually emulates
    log y(log x, pars).

    Attributes:
        logx: bool
            whether to transform x to log scale or not
        logy: bool
            whether to transform y to log scale or not
        epsilon: float
            smallest positive value to actually take the logarithm. If smaller
            than this, it is replaced by log(epsilon). Defaults to 1.25e-12.
    """
    logx = True
    logy = True
    epsilon = 1.25e-12


    def transform_input_x(self, old_x):
        if self.logx:
            x = self.__log_transform(old_x, 0)
        else:
            x = old_x.copy()
        return super(GPLogEmulator, self).transform_input_x(x)


    def transform_input_y_and_yerr(self, old_y, old_yerr):
        if self.logy:
            old_y = old_y.copy()
            old_yerr = old_yerr.copy()
#            if old_y.min() < 0:
#                self.cte = -10. * old_y.min()
#            else:
#                self.cte = 0.
            y = self.__log_transform(old_y, old_yerr)
            yerr = self.__transform_error(old_yerr, old_y)
        else:
#            self.cte = 0.
            y, yerr = old_y, old_yerr

        return super(GPLogEmulator, self).transform_input_y_and_yerr(y, yerr)


    def inverse_transform_output(self, output, covariance):
        output, covariance = super(GPLogEmulator, self).inverse_transform_output(output, covariance)
        variance = np.diagonal(covariance)

        transformed_output = self.__inverse_log_transform(output, variance)
        transformed_covariance = self.__inverse_transform_covariance(covariance, output)

        return transformed_output, transformed_covariance


    @classmethod
    def build_emulator(cls, kernel, initial_conditions=None, par_range=None, ntries=1,
                       white_noise=None, fit_white_noise=False,
                       mean=0., fit_mean=False, solver=None,
                       x_as_index=False, standardize_input=True, logx=True, logy=True,
                       standardize_mean_output=True, standardize_std_output=True,
                       parameter_covariance=None,
                       **kwargs):
        """
        Specifies gaussian process emulation on log space details and returns
        class ready to be instantiated.

        By calling this you are able to specify the kernel, initial conditions and other relevant
        information needed to carry the emulation. You should never actually instantiate this
        class (or the modified version returned by this method). Instead you can use the
        returned class as the emulator_generator attribute of a EmulatedModule subclass,
        and both that class and the emulatortrainer sampler will do the work under the hood.

        You can get a working emulator by specifying only the kernel and one of
        initial_conditions and par_range.

        Parameters:
            kernel:
                a george kernel object with dimensions matched the ones of the parameter space
                you wish to emulate
            initial_conditions: np.ndarray, shaped like (npars, ntries) or (npars,), or None
                array describing the initial conditions to use during training
            par_range: np.ndarray, shaped like (npars, 2), or None
                array describing the limits of the parameter space region intented to be
                emulated. If initial_conditions is absent, it is used to randomly sample
                initial conditions.
            ntries: int
                number of different initial conditions to use when searching the maximum
                likelihood hyperparameters
            white_noise:
                object describing white noise modelling, compatible with george implementation
                (cf. george documentation)
            fit_white_noise: bool
                whether to fit the parameters describing white noise or not.
            mean:
                object describing mean of the gaussian process.
            fit_mean: bool
                whether to fit the parameters describing the mean.
            solver:
                george solver to use for matrix inversions. Defaults to BasicSolver.
            x_as_index: bool
                if True, doesn't treat the x variable of the function being emulated as
                an independent dimension, and instead train a different gaussian process
                for each value of the variable.
            standardize_input: bool
                if True (default), standardize parameters range to lie between 0 and 1
            logx: bool
                if True, perform emulation using log(x) instead
            logy: bool
                if True, perform emulation using log(y) instead
            standardize_mean_output: bool
                if True (default), standardize output by subtracting the mean of training ys
                (after change of variables y -> log y, of course)
            standardize_std_output: bool
                if True (default), standardize output by dividing by the std of training ys
                (after change of variables y -> log y, of course)
            parameter_covariance: np.ndarray or None
                if not None, covariance to be diagonalized to transform input parameters
        """
        cls.logx = logx
        cls.logy = logy

        return super(GPLogEmulator, cls).build_emulator(kernel=kernel,
                                                        initial_conditions=initial_conditions,
                                                        par_range=par_range,
                                                        ntries=ntries,
                                                        white_noise=white_noise,
                                                        fit_white_noise=fit_white_noise,
                                                        mean=mean,
                                                        fit_mean=fit_mean,
                                                        solver=solver,
                                                        x_as_index=x_as_index,
                                                        standardize_input=standardize_input,
                                                        standardize_mean_output=standardize_mean_output,
                                                        standardize_std_output=standardize_std_output,
                                                        parameter_covariance=parameter_covariance,
                                                        **kwargs)


    def __log_transform(self, y, yerr):
        vary = yerr ** 2
        #ret = np.log(y / np.sqrt(1 + (vary / (y**2))))
        self.cte = 0
        ret = np.log(y + self.cte)
        if np.isnan(ret).any():
            print("WARNING: Negative values in y before log transformation.", file=sys.stderr)
        #ret[y < self.epsilon] = np.log(self.epsilon)
        return ret


    def __inverse_log_transform(self, logvalue, logvariance):
        # the mean of the lognormal distribution is biased
        #return np.exp(logvalue + 1. / 2 * logvariance)
        self.cte = 0
        return np.exp(logvalue) - self.cte


    def __transform_error(self, yerr, y):
        logyerr = np.sqrt(self.__transform_variance(yerr**2, y))
        return logyerr


    def __transform_variance(self, var, y):
        self.cte = 0
        logvar = var / ((y + self.cte)**2)
        return logvar


    def __inverse_transform_covariance(self, logcov, logy):
        #y = np.exp(logy)
        self.cte = 0
        y = self.__inverse_log_transform(logy, np.diag(logcov))
        y += self.cte
        y = y.reshape(-1, 1)
        return logcov * np.dot(y, y.T)

