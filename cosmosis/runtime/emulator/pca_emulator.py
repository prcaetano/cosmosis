#coding: utf-8

u"""Definition of :class:`GPPCAEmulator` and :class:`GPPCALogEmulator`."""

from __future__ import print_function
from builtins import object

import sys
import numpy as np
import pickle
import george
import lhsmdu
import os
from astropy.table import Table
from astropy.io import fits
from cosmosis.gaussian_likelihood import GaussianLikelihood
from cosmosis.runtime import parameter
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline
from cosmosis.datablock.cosmosis_py.errors import BlockWrongValueType
from cosmosis.runtime.module import Module
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section
from cosmosis.runtime.declare import declare_module
from cosmosis.runtime.module import Module, MODULE_TYPE_SETUP, MODULE_TYPE_EXECUTE_SIMPLE, MODULE_TYPE_EXECUTE_CONFIG, MODULE_TYPE_CLEANUP, MODULE_LANG_PYTHON
from george.kernels import ExpSquaredKernel
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from scipy.linalg import LinAlgError
from sklearn.decomposition import PCA
from wpca import WPCA
from sklearn import preprocessing
from itertools import repeat

from .emulator import GPEmulator, GPLogEmulator


class GPPCAEmulator(GPEmulator):
    """
    Gaussian Process emulator that performs PCA decomposition on the emulated function.

    This class is intented to emulate functions y(x, pars) by first doing
    a pca decomposition of the y(x) functions and them keeping the npca
    most significant components. Each pca component is treated independently,
    and a total of npca emulators are fitted.

    Attributes:
        npca: (integer of float between 0. and 1.)
            number of pca components to keep.
            If float, describes the fraction of the cummulative
            explained variance to keep.
        standardize_mean_pca: bool
            If true, input data is normalized to 0 mean before doing pca
        standardize_std_pca: bool
            If true, input data is normalized to unit variance before doing pca
        whiten_pca: bool
            If true, whitening is enabled during PCA (cf. docs of sklearn.decomposition.PCA)
    """
    npca = None
    standardize_mean_pca = True
    standardize_std_pca = True
    whiten_pca = False


    def transform_input_x(self, old_x):
        x = np.arange(len(self.pca.components_))
        return super(GPPCAEmulator, self).transform_input_x(x)


    def transform_input_y_and_yerr(self, old_y, old_yerr):

        try:
            y = self.pca_scaler.transform(old_y)
            yerr = old_yerr / self.pca_scaler.scale_
        except AttributeError:
            self.pca_scaler = preprocessing.StandardScaler(with_mean=self.standardize_mean_pca,
                                                           with_std=self.standardize_std_pca).fit(old_y)
            y = self.pca_scaler.transform(old_y)
            yerr = old_yerr / self.pca_scaler.scale_

        if self.pca_implementation == "wpca":
            if self.whiten_pca:
                raise NotImplemented("Whitening not implemented for WPCA transformer.")
            ## Very small uncertainties may cause infinite weights
            ## so we add an epsilon to prevent overflows
            yerr += np.finfo(yerr.dtype).eps
            weights = 1. / yerr
            weights[np.isnan(yerr)] = 0.
            weights[np.isnan(y)] = 0.
            try:
                y = self.pca.transform(y, weights=weights)
            except AttributeError:
                self.pca = WPCA(self.npca).fit(y, weights=weights)
                y = self.pca.transform(y, weights=weights)
        elif self.pca_implementation == "pca":
            if np.isnan(y).any():
                print("WARNING: during PCA, y values contain nan values. "
                      "Setting those to 1 / epsilon, but you really should be using weighted PCA "
                      "or handling those values appropriatelly.",
                      file=sys.stderr)
                y[np.isnan(y)] = 1./np.finfo(y.dtype).eps
            try:
                y = self.pca.transform(y)
            except AttributeError:
                self.pca = PCA(self.npca, whiten=self.whiten_pca).fit(y)
                y = self.pca.transform(y)
        else:
            raise RuntimeError("The pca implementation choice "
                               "{} is not valid.".format(self.pca_implementation))

        principal_components = self.pca.components_
        explained_variance = self.pca.explained_variance_

        yerrs = []
        for yerr_ in yerr:
            principal_components = principal_components.copy()
            if self.whiten_pca:
                principal_components /= np.sqrt(explained_variance[:, np.newaxis])
            yerrs.append(np.sqrt(np.diagonal(np.matmul(np.matmul(principal_components,
                                                                 np.diag(yerr_**2)),
                                                       principal_components.T))))
        yerr = np.array(yerrs)

        return super(GPPCAEmulator, self).transform_input_y_and_yerr(y, yerr)


    def inverse_transform_output(self, output, covariance):
        output, covariance = super(GPPCAEmulator, self).inverse_transform_output(output, covariance)
        variance = np.diagonal(covariance)

        if self.pca_implementation == "wpca":
            transformed_output = self.pca_scaler.inverse_transform(
                                   self.pca.inverse_transform(output.reshape(1,-1))).flatten()
        else:
            transformed_output = self.pca_scaler.inverse_transform(
                                   self.pca.inverse_transform(output))

        principal_components = self.pca.components_.copy()
        explained_variance = self.pca.explained_variance_
        if self.whiten_pca:
            principal_components *= np.sqrt(explained_variance[:, np.newaxis])
        transformed_covariance = np.matmul(np.matmul(principal_components.T,
                                                     covariance),
                                           principal_components)

        if self.pca_scaler.scale_ is not None:
            scaling = np.matmul(self.pca_scaler.scale_.reshape(-1,1),
                                self.pca_scaler.scale_.reshape(1,-1))
            transformed_covariance *= scaling

        return transformed_output, transformed_covariance


    @classmethod
    def build_emulator(cls, kernel, initial_conditions=None, par_range=None, ntries=1,
                       white_noise=None, fit_white_noise=False,
                       mean=0., fit_mean=False, solver=None,
                       npca=0.99, standardize_input=True, standardize_mean_pca=True,
                       standardize_std_pca=True, whiten_pca=False, pca_implementation="pca",
                       standardize_mean_output=True, standardize_std_output=True,
                       parameter_covariance=None,
                       **kwargs):
        """
        Specifies gaussian process emulation with PCA details and returns
        class ready to be instantiated.

        By calling this you are able to specify the kernel, initial conditions and other relevant
        information needed to carry the emulation. You should never actually instantiate this
        class (or the modified version returned by this method). Instead you can use the
        returned class as the emulator_generator attribute of a EmulatedModule subclass,
        and both that class and the emulatortrainer sampler will do the work under the hood.

        You can get a working emulator by specifying only the kernel and one of
        initial_conditions and par_range.

        Parameters:
            kernel:
                a george kernel object with dimensions matched the ones of the parameter space
                you wish to emulate
            initial_conditions: np.ndarray, shaped like (npars, ntries) or (npars,), or None
                array describing the initial conditions to use during training
            par_range: np.ndarray, shaped like (npars, 2), or None
                array describing the limits of the parameter space region intented to be
                emulated. If initial_conditions is absent, it is used to randomly sample
                initial conditions.
            ntries: int
                number of different initial conditions to use when searching the maximum
                likelihood hyperparameters
            white_noise:
                object describing white noise modelling, compatible with george implementation
                (cf. george documentation)
            fit_white_noise: bool
                whether to fit the parameters describing white noise or not.
            mean:
                object describing mean of the gaussian process.
            fit_mean: bool
                whether to fit the parameters describing the mean.
            solver:
                george solver to use for matrix inversions. Defaults to BasicSolver.
            logx: bool
                if True, perform emulation using log(x) instead
            logy: bool
                if True, perform emulation using log(y) instead
            npca: integer of float between 0. and 1.
                if integer, keep only the npca most significant principal components.
                If float, keep components holding at least npca cumulative explained variance.
            standardize_input: bool
                if True (default), standardize parameters range to lie between 0 and 1
            standardize_mean_pca: bool
                if True, subtract mean before doing PCA
            standardize_std_pca: bool
                if True, divide by standard deviation before doing PCA
            whiten_pca: bool
                If True, whitening is enabled during PCA (cf. sklearn.decomposition.PCA docs)
            pca_implementation: str
                Specifies which implementation of PCA to use. Possible choices include `pca`
                (for sklearn.decomposition.PCA) and `wpca` (for wpca.WPCA, from wpca package).
                Defaults to `pca`.
            standardize_mean_output: bool
                if True (default), standardize output by subtracting the mean of training ys
                (after PCA)
            standardize_std_output: bool
                if True (default), standardize output by dividing by the std of training ys
                (after PCA)
            parameter_covariance: np.ndarray or None
                if not None, covariance to be diagonalized to transform input parameters
        """
        cls.npca = npca
        cls.standardize_mean_pca = standardize_mean_pca
        cls.standardize_std_pca = standardize_std_pca
        cls.whiten_pca = whiten_pca
        cls.pca_implementation = pca_implementation

        if "x_as_index" in kwargs:
            kwargs.pop("x_as_index")
        return super(GPPCAEmulator, cls).build_emulator(kernel=kernel,
                                                        initial_conditions=initial_conditions,
                                                        par_range=par_range,
                                                        ntries=ntries,
                                                        white_noise=white_noise,
                                                        fit_white_noise=fit_white_noise,
                                                        mean=mean,
                                                        fit_mean=fit_mean,
                                                        solver=solver,
                                                        x_as_index=True,
                                                        standardize_input=standardize_input,
                                                        standardize_mean_output=standardize_mean_output,
                                                        standardize_std_output=standardize_std_output,
                                                        parameter_covariance=parameter_covariance,
                                                        **kwargs)



class GPPCALogEmulator(GPLogEmulator, GPPCAEmulator):
    """
    Gaussian Process emulator that performs PCA decomposition on the emulated function, in log space.

    This class is intented to emulate functions y(x, pars) by first doing
    a pca decomposition of the log y(log x) functions and them keeping the npca
    most significant components. Each pca component is treated independently,
    and a total of npca emulators are fitted.

    Attributes:
        npca: (integer of float between 0. and 1.)
            number of pca components to keep.
            If float, describes the fraction of the cummulative
            explained variance to keep
        logx: bool
            whether or not to take the log of x variable
        logy: bool
            whether or not to take the log of y variable

    """


    @classmethod
    def build_emulator(cls, kernel, initial_conditions=None, par_range=None, ntries=1,
                       white_noise=None, fit_white_noise=False,
                       mean=0., fit_mean=False, solver=None,
                       logx=True, logy=True, npca=0.99, standardize_input=True,
                       standardize_mean_pca=True, standardize_std_pca=True,
                       whiten_pca=False, pca_implementation="pca",
                       standardize_mean_output=True, standardize_std_output=True,
                       parameter_covariance=None,
                       **kwargs):
        """
        Specifies gaussian process emulation with PCA in log space
        details and returns class ready to be instantiated.

        By calling this you are able to specify the kernel, initial conditions and other relevant
        information needed to carry the emulation. You should never actually instantiate this
        class (or the modified version returned by this method). Instead you can use the
        returned class as the emulator_generator attribute of a EmulatedModule subclass,
        and both that class and the emulatortrainer sampler will do the work under the hood.

        You can get a working emulator by specifying only the kernel and one of
        initial_conditions and par_range.

        Parameters:
            kernel:
                a george kernel object with dimensions matched the ones of the parameter space
                you wish to emulate
            initial_conditions: np.ndarray, shaped like (npars, ntries) or (npars,), or None
                array describing the initial conditions to use during training
            par_range: np.ndarray, shaped like (npars, 2), or None
                array describing the limits of the parameter space region intented to be
                emulated. If initial_conditions is absent, it is used to randomly sample
                initial conditions.
            ntries: int
                number of different initial conditions to use when searching the maximum
                likelihood hyperparameters
            white_noise:
                object describing white noise modelling, compatible with george implementation
                (cf. george documentation)
            fit_white_noise: bool
                whether to fit the parameters describing white noise or not.
            mean:
                object describing mean of the gaussian process.
            fit_mean: bool
                whether to fit the parameters describing the mean.
            solver:
                george solver to use for matrix inversions. Defaults to BasicSolver.
            logx: bool
                if True, perform emulation using log(x) instead
            logy: bool
                if True, perform emulation using log(y) instead
            npca: integer of float between 0. and 1.
                if integer, keep only the npca most significant principal components.
                If float, keep components holding at least npca cumulative explained variance.
            standardize_input: bool
                if True (default), standardize parameters range to lie between 0 and 1
            standardize_mean_pca: bool
                if True, subtract mean before doing PCA
            standardize_std_pca: bool
                if True, divide by standard deviation before doing PCA
            whiten_pca: bool
                If True, whitening is enabled during PCA (cf. sklearn.decomposition.PCA docs)
            pca_implementation: str
                Specifies which implementation of PCA to use. Possible choices include `pca`
                (for sklearn.decomposition.PCA) and `wpca` (for wpca.WPCA, from wpca package).
                Defaults to `pca`.
            standardize_mean_output: bool
                if True (default), standardize output by subtracting the mean of training ys
                (after PCA and log transforms)
            standardize_std_output: bool
                if True (default), standardize output by dividing by the std of training ys
                (after PCA and log transforms)
            parameter_covariance: np.ndarray or None
                if not None, covariance to be diagonalized to transform input parameters

        """
        return super(GPPCALogEmulator, cls).build_emulator(kernel=kernel,
                                                           initial_conditions=initial_conditions,
                                                           par_range=par_range,
                                                           ntries=ntries,
                                                           white_noise=white_noise,
                                                           fit_white_noise=fit_white_noise,
                                                           mean=mean,
                                                           fit_mean=fit_mean,
                                                           solver=solver,
                                                           logx=logx,
                                                           logy=logy,
                                                           npca=npca,
                                                           standardize_input=standardize_input,
                                                           standardize_mean_pca=standardize_mean_pca,
                                                           standardize_std_pca=standardize_std_pca,
                                                           whiten_pca=whiten_pca,
                                                           pca_implementation=pca_implementation,
                                                           standardize_mean_output=standardize_mean_output,
                                                           standardize_std_output=standardize_std_output,
                                                           parameter_covariance=parameter_covariance,
                                                           **kwargs)

